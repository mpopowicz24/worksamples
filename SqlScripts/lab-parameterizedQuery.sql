Declare @minValue smallmoney
declare @maxValue smallmoney

set @minValue = 0
set @maxValue = 10000

select * 
from [grant]
where Amount between @minValue and @maxValue