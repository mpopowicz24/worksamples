select max(UnitPrice) as MaxPrice, Categories.CategoryID
from Products
inner join Categories
on Categories.CategoryID = Products.CategoryID
group by Categories.CategoryID

select max(y.CountOrders) as maxCount
from (
select count(OrderID) as CountOrders, orders.CustomerID
from Customers
inner join Orders
on Orders.CustomerID = Customers.CustomerID
group by Orders.CustomerID) as y


select Top 1 count(OrderID) as CountOrders, orders.CustomerID
from Customers
inner join Orders
on Orders.CustomerID = Customers.CustomerID
group by Orders.CustomerID
order by CountOrders desc

select Customers.CustomerID, ([Order Details].Quantity * [Order Details].UnitPrice) as totalValue
from Customers
inner join Orders
on Orders.CustomerID = Customers.CustomerID
inner join [Order Details]
on [Order Details].OrderID = orders.OrderID
order by totalValue desc

select count(OrderID) as CountOrders, orders.CustomerID
from Customers
inner join Orders
on Orders.CustomerID = Customers.CustomerID
group by orders.CustomerID
having count(orderID) > 10
order by CountOrders

select (LastName + ', ' + FirstName) as EmployeeName, count(OrderID) as OrderCount
from Employees
inner join Orders
on Orders.EmployeeID = Employees.EmployeeID
group by (LastName + ', ' + FirstName)
having count(OrderID) > 100







