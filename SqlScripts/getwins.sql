
select wins.wins, losses.losses 
from
	(select count(*) as wins
	from GameInfo a
	inner join GameInfo b
	on a.GameId = b.GameId and a.TeamId <> b.TeamId and a.TeamId = 2
	where a.GameId in
		(select GameInfo.GameId 
		from GameInfo 
		where GameInfo.TeamId = 2
		and gameinfo.Score is not null)
	and a.Score > b.Score) as wins,
	
	(select count(*) as losses
	from GameInfo a
	inner join GameInfo b
	on a.GameId = b.GameId and a.TeamId <> b.TeamId and a.TeamId = 2
	where a.GameId in
		(select GameInfo.GameId 
		from GameInfo 
		where GameInfo.TeamId = 2
		and gameinfo.Score is not null)
	and a.Score < b.Score) as losses
