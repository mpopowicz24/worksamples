create procedure GetProductListByCategory
(
	@categoryName nvarchar(max)
) as

select *
from CurrentProducts
where Category = @categoryName

go

create procedure GetGrantsByEmployee
(
	@lastName varchar(30)
) as

select FirstName, LastName, GrantName, Amount
from Employee
inner join [Grant]
on [Grant].EmpID = Employee.EmpID
where LastName = @lastName

go

create procedure InsertGrant
(
	@grantName varchar(50),
	@EmpID int,
	@Amount smallmoney
) as

Insert into [grant]
(GrantName, EmpID, Amount)
values (@grantName, @EmpID, @Amount)

go

create procedure UpdateGrant
(
	@grantID int,
	@grantName varchar(50),	
	@Amount smallmoney
) as

update [grant] 
set GrantName = @grantName, Amount = @Amount
where GrantID = @GrantID

go

