select Employees.FirstName, Employees.LastName, Territories.TerritoryDescription
from Employees
inner join EmployeeTerritories
on Employees.EmployeeID = EmployeeTerritories.EmployeeID
inner join Territories
on EmployeeTerritories.TerritoryID = Territories.TerritoryID


select Customers.CompanyName, orders.OrderDate, Products.ProductName, Customers.Country
from Customers
inner join orders
on orders.CustomerID = Customers.CustomerID
inner join [Order Details]
on [Order Details].OrderID = Orders.OrderID
inner join products
on Products.ProductID = [Order Details].ProductID
where Customers.Country = 'USA'

select orders.*, [Order Details].*, Products.ProductName 
from Orders
inner join [Order Details]
on [Order Details].OrderID = orders.OrderID
inner join products 
on [Order Details].ProductID = Products.ProductID
where Products.ProductName='chai'

