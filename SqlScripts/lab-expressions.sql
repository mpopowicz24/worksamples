select ProductName, (UnitPrice * UnitsInStock) as value
from products
order by value desc

select (LastName+', '+ FirstName) as NameLastFirst
from Employees
order by LastName, FirstName

select ProductName, (UnitPrice * UnitsInStock) as value,
(UnitPrice * UnitsInStock * 1.29) as valueCAD,
(UnitPrice * UnitsInStock * .88) as valueEUR,
(UnitPrice * UnitsInStock * 106.55) as valueJPY,
(UnitPrice * UnitsInStock * 18.60) as valueMXN
from products
order by value desc