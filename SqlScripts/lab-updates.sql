select * from Employee
where EmpID = 11

update Employee
set LastName = 'Green'
where EmpID = 11

select * from Employee
inner join Location
on Employee.LocationID = Location.LocationID
where location.City = 'Spokane'

update Employee
set Status = 'External'
from Employee
inner join Location
on Employee.LocationID = Location.LocationID
where location.City = 'Spokane'

select * from location where City = 'seattle'

update Location
set Street = '111 1st Ave.'
where city = 'Seattle'

select * 
from Employee
inner join Location
on Location.LocationID = Employee.LocationID
inner join [Grant]
on [grant].EmpID = Employee.EmpID
where city = 'Boston'

update [grant]
set Amount = 20000
from Employee
inner join Location
on Location.LocationID = Employee.LocationID
inner join [Grant]
on [grant].EmpID = Employee.EmpID
where city = 'Boston'
