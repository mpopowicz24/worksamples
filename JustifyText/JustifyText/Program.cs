﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JustifyText.BLL;

namespace JustifyText
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] wordsArray =
            {
                "This", "is", "an", "example", "of", "text", "justification.",
                "Here", "is", "a", "second", "sentence.", "And", "third."
            };

            List<string> linesList = Converter.JustifyText(wordsArray, 30);
           

            foreach (var line in linesList)
            {
                Console.WriteLine(line);
            }


            Console.ReadLine();

        }

    }
}