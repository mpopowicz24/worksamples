﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace JustifyText.BLL
{
    public class Converter
    {
        
        /// <summary>
        /// Returns justified text
        /// </summary>
        /// <param name="words">An array of strings</param>
        /// <param name="width">The width of the container</param>
        /// <returns></returns>
        public static List<string> JustifyText(string[] words, int width)
        {
            List<string> list = ConvertWordsToLines(words, width);
            return FormatText(list, width);
        }
        
        private static List<string> ConvertWordsToLines(string[] words, int width)
        {
            List<string> linesList = new List<string>();
            string line = "";
            int lineLength = 0;
            
            for (int i = 0; i < words.Length; i++)
            {
                //if word is same length as line width
                if (words[i].Length == width)
                    lineLength += width;
                else
                    lineLength += words[i].Length + 1;

                if (lineLength > width)
                {
                    linesList.Add(line.Trim());
                    line = string.Empty;
                    i--;
                    lineLength = 0;
                }
                else
                    line += words[i] + " ";
            }
            //add the last line to the list
            if (line != string.Empty)
                linesList.Add(line.Trim());

            return linesList;
        }

        private static List<string> FormatText(List<string> lines, int width)
        {
            List<string> newList = new List<string>();

            for (int i=0; i < lines.Count; i++)
            {
                //justify all but last line
                if (i < lines.Count - 1)
                {
                    string newLine = JustifyLine(lines[i], width);
                    newList.Add(newLine);
                }
                else //last line is left justified
                    newList.Add(lines[i].Trim('\0'));
            }

            return newList;
        }

        private static string JustifyLine(string line, int width)
        {
            List<int> spaceIndicies = new List<int>();
            int requiredExtraSpaces = width - line.Length;
            char[] newLine = new char[width];

            line.CopyTo(0, newLine, 0, line.Length);

            //get index of spaces in line
            for (int j = 0; j < line.Length; j++)
            {
                if (line[j] == ' ')
                    spaceIndicies.Add(j);
            }

            int iteration = 1;

            while (requiredExtraSpaces > 0)
            {
                if (spaceIndicies.Count > 0)
                {
                    for (int j = 0; j < spaceIndicies.Count; j++)
                    {
                        for (int k = newLine.Length - 1; k > spaceIndicies[j] + (iteration * j); k--)
                        {
                            newLine[k] = newLine[k - 1];
                        }

                        requiredExtraSpaces--;
                        if (requiredExtraSpaces == 0)
                            break;
                    }
                }
                else
                    requiredExtraSpaces = 0;
                
                iteration++;
            }

            return new string(newLine).Trim('\0');
        }
         
    }
}
