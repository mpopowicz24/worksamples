System.register(['../shared/recipe', '../shared/ingredient'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var recipe_1, ingredient_1;
    var RECIPES;
    return {
        setters:[
            function (recipe_1_1) {
                recipe_1 = recipe_1_1;
            },
            function (ingredient_1_1) {
                ingredient_1 = ingredient_1_1;
            }],
        execute: function() {
            exports_1("RECIPES", RECIPES = [
                new recipe_1.Recipe('Wiener Schnitzel', 'A tasty Schnitzel', 'http://images.derberater.de/files/imagecache/456xXXX_berater/berater/slides/WienerSchnitzel.jpg', [
                    new ingredient_1.Ingredient('Pork Meat', 1),
                    new ingredient_1.Ingredient('French Fries', 1),
                    new ingredient_1.Ingredient('Salad', 2),
                ]),
                new recipe_1.Recipe('Super Burger', 'Tastes so delicious!', 'http://www.fraeuleinburger.de/tl_files/images/content/burger/Fraeulein-Burger.jpg', [
                    new ingredient_1.Ingredient('Buns', 4),
                    new ingredient_1.Ingredient('Salad', 1),
                    new ingredient_1.Ingredient('Paddies', 4),
                    new ingredient_1.Ingredient('Vegetabels', 2)
                ])
            ]);
        }
    }
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1vY2svcmVjaXBlcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O1FBR1csT0FBTzs7Ozs7Ozs7OztZQUFQLHFCQUFBLE9BQU8sR0FBYTtnQkFDMUIsSUFBSSxlQUFNLENBQUMsa0JBQWtCLEVBQzFCLG1CQUFtQixFQUNuQixpR0FBaUcsRUFDakc7b0JBQ0ksSUFBSSx1QkFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7b0JBQzlCLElBQUksdUJBQVUsQ0FBQyxjQUFjLEVBQUUsQ0FBQyxDQUFDO29CQUNqQyxJQUFJLHVCQUFVLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztpQkFDN0IsQ0FDSjtnQkFDRCxJQUFJLGVBQU0sQ0FBQyxjQUFjLEVBQ3JCLHNCQUFzQixFQUN0QixtRkFBbUYsRUFDbkY7b0JBQ0EsSUFBSSx1QkFBVSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7b0JBQ3pCLElBQUksdUJBQVUsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO29CQUMxQixJQUFJLHVCQUFVLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQztvQkFDNUIsSUFBSSx1QkFBVSxDQUFDLFlBQVksRUFBRSxDQUFDLENBQUM7aUJBQ2xDLENBQUM7YUFFTCxDQUFBLENBQUMiLCJmaWxlIjoibW9jay9yZWNpcGVzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtSZWNpcGV9IGZyb20gJy4uL3NoYXJlZC9yZWNpcGUnXHJcbmltcG9ydCB7SW5ncmVkaWVudH0gZnJvbSAnLi4vc2hhcmVkL2luZ3JlZGllbnQnXHJcblxyXG5leHBvcnQgbGV0IFJFQ0lQRVM6IFJlY2lwZVtdID0gW1xyXG4gICAgIG5ldyBSZWNpcGUoJ1dpZW5lciBTY2huaXR6ZWwnLFxyXG4gICAgICAgICdBIHRhc3R5IFNjaG5pdHplbCcsXHJcbiAgICAgICAgJ2h0dHA6Ly9pbWFnZXMuZGVyYmVyYXRlci5kZS9maWxlcy9pbWFnZWNhY2hlLzQ1NnhYWFhfYmVyYXRlci9iZXJhdGVyL3NsaWRlcy9XaWVuZXJTY2huaXR6ZWwuanBnJyxcclxuICAgICAgICBbXHJcbiAgICAgICAgICAgIG5ldyBJbmdyZWRpZW50KCdQb3JrIE1lYXQnLCAxKSxcclxuICAgICAgICAgICAgbmV3IEluZ3JlZGllbnQoJ0ZyZW5jaCBGcmllcycsIDEpLFxyXG4gICAgICAgICAgICBuZXcgSW5ncmVkaWVudCgnU2FsYWQnLCAyKSxcclxuICAgICAgICBdXHJcbiAgICApLFxyXG4gICAgbmV3IFJlY2lwZSgnU3VwZXIgQnVyZ2VyJywgXHJcbiAgICAgICAgJ1Rhc3RlcyBzbyBkZWxpY2lvdXMhJywgXHJcbiAgICAgICAgJ2h0dHA6Ly93d3cuZnJhZXVsZWluYnVyZ2VyLmRlL3RsX2ZpbGVzL2ltYWdlcy9jb250ZW50L2J1cmdlci9GcmFldWxlaW4tQnVyZ2VyLmpwZycsIFxyXG4gICAgICAgIFtcclxuICAgICAgICBuZXcgSW5ncmVkaWVudCgnQnVucycsIDQpLFxyXG4gICAgICAgIG5ldyBJbmdyZWRpZW50KCdTYWxhZCcsIDEpLFxyXG4gICAgICAgIG5ldyBJbmdyZWRpZW50KCdQYWRkaWVzJywgNCksXHJcbiAgICAgICAgbmV3IEluZ3JlZGllbnQoJ1ZlZ2V0YWJlbHMnLCAyKVxyXG4gICAgXSlcclxuICAgIFxyXG5dOyJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
