System.register(['angular2/core', './recipe.service', 'angular2/router', 'angular2/common'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, recipe_service_1, router_1, common_1;
    var RecipeEditComponent;
    function hasNumbers(control) {
        if (!('' + control.value).match('\\d+')) {
            return { noNumbers: true };
        }
    }
    function greaterZero(control) {
        if (!(+control.value > 0)) {
            return { tooSmall: true };
        }
    }
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (recipe_service_1_1) {
                recipe_service_1 = recipe_service_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            }],
        execute: function() {
            RecipeEditComponent = (function () {
                function RecipeEditComponent(_routeParams, _router, _recipesService, _formBuilder) {
                    this._routeParams = _routeParams;
                    this._router = _router;
                    this._recipesService = _recipesService;
                    this._formBuilder = _formBuilder;
                    this._editMode = 'create';
                    this._submitted = false;
                }
                RecipeEditComponent.prototype.onAddItem = function (itemName, itemAmount) {
                    this.myForm.controls['ingredients'].push(new common_1.ControlGroup({
                        name: new common_1.Control(itemName, common_1.Validators.required),
                        amount: new common_1.Control(itemAmount, common_1.Validators.compose([
                            common_1.Validators.required, hasNumbers, greaterZero
                        ]))
                    }));
                };
                RecipeEditComponent.prototype.onRemoveItem = function (index) {
                    this.myForm.controls['ingredients'].removeAt(index);
                };
                RecipeEditComponent.prototype.onSubmit = function () {
                    //this works because form matches recipe class
                    this.recipe = this.myForm.value;
                    if (this._editMode === 'edit') {
                        this._recipesService.updateRecipe(this._recipeIndex, this.recipe);
                    }
                    else {
                        this._recipesService.insertRecipe(this.recipe);
                    }
                    this._submitted = true;
                    this.navigateBack();
                };
                RecipeEditComponent.prototype.onCancel = function () {
                    this.navigateBack();
                };
                RecipeEditComponent.prototype.navigateBack = function () {
                    this._router.navigate(['RecipeDetail', { recipeIndex: this._recipeIndex }]);
                };
                RecipeEditComponent.prototype.ngOnInit = function () {
                    this._editMode = this._routeParams.get('editMode');
                    var fbRecipeName = '';
                    var fbRecipeImageUrl = '';
                    var fbRecipeContent = '';
                    var fbIngredients = new common_1.ControlArray([]);
                    if (this._editMode === 'edit') {
                        this._recipeIndex = +this._routeParams.get('itemIndex');
                        this.recipe = this._recipesService.getRecipe(this._recipeIndex);
                        for (var i = 0; i < this.recipe.ingredients.length; i++) {
                            fbIngredients.push(new common_1.ControlGroup({
                                name: new common_1.Control(this.recipe.ingredients[i].name, common_1.Validators.required),
                                amount: new common_1.Control(this.recipe.ingredients[i].amount, common_1.Validators.compose([
                                    common_1.Validators.required, hasNumbers, greaterZero
                                ]))
                            }));
                            fbRecipeName = this.recipe.name;
                            fbRecipeImageUrl = this.recipe.imageUrl;
                            fbRecipeContent = this.recipe.content;
                        }
                    }
                    this.myForm = this._formBuilder.group({
                        name: [fbRecipeName, common_1.Validators.required],
                        imageUrl: [fbRecipeImageUrl],
                        content: [fbRecipeContent, common_1.Validators.required],
                        ingredients: this._formBuilder.array(fbIngredients.controls)
                    });
                };
                RecipeEditComponent.prototype.routerCanDeactivate = function (nextInstruction, previousInstruction) {
                    if (this._submitted || this.myForm.pristine) {
                        return true;
                    }
                    return confirm('Do you want to leave');
                };
                RecipeEditComponent = __decorate([
                    core_1.Component({
                        templateUrl: '../templates/recipes/recipe-edit.tpl.html'
                    }), 
                    __metadata('design:paramtypes', [router_1.RouteParams, router_1.Router, recipe_service_1.RecipeService, common_1.FormBuilder])
                ], RecipeEditComponent);
                return RecipeEditComponent;
            }());
            exports_1("RecipeEditComponent", RecipeEditComponent);
        }
    }
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInJlY2lwZS1ib29rL3JlY2lwZS1lZGl0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7OztJQW9HQSxvQkFBb0IsT0FBZ0I7UUFDaEMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsR0FBRyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUEsQ0FBQztZQUNyQyxNQUFNLENBQUMsRUFBQyxTQUFTLEVBQUUsSUFBSSxFQUFDLENBQUM7UUFDN0IsQ0FBQztJQUNMLENBQUM7SUFFRCxxQkFBcUIsT0FBZ0I7UUFDakMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFBLENBQUM7WUFDdkIsTUFBTSxDQUFDLEVBQUMsUUFBUSxFQUFFLElBQUksRUFBQyxDQUFDO1FBQzVCLENBQUM7SUFDTCxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7O1lBcEdEO2dCQU9JLDZCQUFvQixZQUF5QixFQUFVLE9BQWUsRUFDOUQsZUFBOEIsRUFBVSxZQUF5QjtvQkFEckQsaUJBQVksR0FBWixZQUFZLENBQWE7b0JBQVUsWUFBTyxHQUFQLE9BQU8sQ0FBUTtvQkFDOUQsb0JBQWUsR0FBZixlQUFlLENBQWU7b0JBQVUsaUJBQVksR0FBWixZQUFZLENBQWE7b0JBTGpFLGNBQVMsR0FBRSxRQUFRLENBQUM7b0JBRXBCLGVBQVUsR0FBRyxLQUFLLENBQUM7Z0JBR2dELENBQUM7Z0JBRTVFLHVDQUFTLEdBQVQsVUFBVSxRQUFnQixFQUFFLFVBQWtCO29CQUMzQixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUUsQ0FBQyxJQUFJLENBQ3RELElBQUkscUJBQVksQ0FBQzt3QkFDWCxJQUFJLEVBQUUsSUFBSSxnQkFBTyxDQUFDLFFBQVEsRUFBRSxtQkFBVSxDQUFDLFFBQVEsQ0FBQzt3QkFDaEQsTUFBTSxFQUFFLElBQUksZ0JBQU8sQ0FBQyxVQUFVLEVBQUUsbUJBQVUsQ0FBQyxPQUFPLENBQUM7NEJBQ25ELG1CQUFVLENBQUMsUUFBUSxFQUFFLFVBQVUsRUFBRSxXQUFXO3lCQUMzQyxDQUFDLENBQUM7cUJBQ1IsQ0FBQyxDQUNILENBQUM7Z0JBQ04sQ0FBQztnQkFFRCwwQ0FBWSxHQUFaLFVBQWEsS0FBYTtvQkFDUCxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUUsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3hFLENBQUM7Z0JBRUQsc0NBQVEsR0FBUjtvQkFDSSw4Q0FBOEM7b0JBQzlDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7b0JBQ2hDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLEtBQUssTUFBTSxDQUFDLENBQUEsQ0FBQzt3QkFDM0IsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7b0JBQ3RFLENBQUM7b0JBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ0osSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO29CQUNuRCxDQUFDO29CQUNELElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO29CQUN2QixJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7Z0JBQ3hCLENBQUM7Z0JBRUQsc0NBQVEsR0FBUjtvQkFDSSxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7Z0JBQ3hCLENBQUM7Z0JBRU8sMENBQVksR0FBcEI7b0JBQ0ksSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxjQUFjLEVBQUUsRUFBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFlBQVksRUFBQyxDQUFDLENBQUMsQ0FBQztnQkFDOUUsQ0FBQztnQkFFRCxzQ0FBUSxHQUFSO29CQUNJLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUM7b0JBQ25ELElBQUksWUFBWSxHQUFHLEVBQUUsQ0FBQztvQkFDdEIsSUFBSSxnQkFBZ0IsR0FBRyxFQUFFLENBQUM7b0JBQzFCLElBQUksZUFBZSxHQUFHLEVBQUUsQ0FBQztvQkFDekIsSUFBSSxhQUFhLEdBQWlCLElBQUkscUJBQVksQ0FBQyxFQUFFLENBQUMsQ0FBQztvQkFFdkQsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsS0FBSyxNQUFNLENBQUMsQ0FBQSxDQUFDO3dCQUMzQixJQUFJLENBQUMsWUFBWSxHQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7d0JBQ3hELElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO3dCQUVoRSxHQUFHLENBQUMsQ0FBRSxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBQyxDQUFDOzRCQUN0RCxhQUFhLENBQUMsSUFBSSxDQUNkLElBQUkscUJBQVksQ0FDWjtnQ0FDSSxJQUFJLEVBQUUsSUFBSSxnQkFBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxtQkFBVSxDQUFDLFFBQVEsQ0FBQztnQ0FDdkUsTUFBTSxFQUFFLElBQUksZ0JBQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEVBQUUsbUJBQVUsQ0FBQyxPQUFPLENBQUM7b0NBQ3ZFLG1CQUFVLENBQUMsUUFBUSxFQUFFLFVBQVUsRUFBRSxXQUFXO2lDQUM5QyxDQUFDLENBQUM7NkJBQ04sQ0FDSixDQUNKLENBQUM7NEJBQ0YsWUFBWSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDOzRCQUNoQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQzs0QkFDeEMsZUFBZSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDO3dCQUMxQyxDQUFDO29CQUVMLENBQUM7b0JBQ0QsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQzt3QkFDbEMsSUFBSSxFQUFFLENBQUMsWUFBWSxFQUFFLG1CQUFVLENBQUMsUUFBUSxDQUFDO3dCQUN6QyxRQUFRLEVBQUUsQ0FBQyxnQkFBZ0IsQ0FBQzt3QkFDNUIsT0FBTyxFQUFFLENBQUMsZUFBZSxFQUFFLG1CQUFVLENBQUMsUUFBUSxDQUFDO3dCQUMvQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQztxQkFFL0QsQ0FBQyxDQUFDO2dCQUNQLENBQUM7Z0JBRUQsaURBQW1CLEdBQW5CLFVBQW9CLGVBQXFDLEVBQUUsbUJBQXlDO29CQUNoRyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUEsQ0FBQzt3QkFDekMsTUFBTSxDQUFDLElBQUksQ0FBQztvQkFDaEIsQ0FBQztvQkFDRCxNQUFNLENBQUMsT0FBTyxDQUFDLHNCQUFzQixDQUFDLENBQUM7Z0JBQzNDLENBQUM7Z0JBM0ZMO29CQUFDLGdCQUFTLENBQUM7d0JBQ1AsV0FBVyxFQUFFLDJDQUEyQztxQkFFM0QsQ0FBQzs7dUNBQUE7Z0JBeUZGLDBCQUFDO1lBQUQsQ0F4RkEsQUF3RkMsSUFBQTtZQXhGRCxxREF3RkMsQ0FBQSIsImZpbGUiOiJyZWNpcGUtYm9vay9yZWNpcGUtZWRpdC5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgT25Jbml0fSBmcm9tICdhbmd1bGFyMi9jb3JlJztcclxuaW1wb3J0IHtSZWNpcGV9IGZyb20gJy4uL3NoYXJlZC9yZWNpcGUnXHJcbmltcG9ydCB7UmVjaXBlU2VydmljZX0gZnJvbSAnLi9yZWNpcGUuc2VydmljZSc7XHJcbmltcG9ydCB7Um91dGVQYXJhbXMsIFJvdXRlciwgQ2FuRGVhY3RpdmF0ZSwgQ29tcG9uZW50SW5zdHJ1Y3Rpb259IGZyb20gJ2FuZ3VsYXIyL3JvdXRlcic7XHJcbmltcG9ydCB7Q29udHJvbEdyb3VwLCBDb250cm9sQXJyYXksIENvbnRyb2wsIFZhbGlkYXRvcnMsIEZvcm1CdWlsZGVyfSBmcm9tICdhbmd1bGFyMi9jb21tb24nO1xyXG5cclxuQENvbXBvbmVudCh7ICAgIFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuLi90ZW1wbGF0ZXMvcmVjaXBlcy9yZWNpcGUtZWRpdC50cGwuaHRtbCcgICAgICBcclxuICAgICAgXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBSZWNpcGVFZGl0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBDYW5EZWFjdGl2YXRle1xyXG4gICAgbXlGb3JtOiBDb250cm9sR3JvdXA7XHJcbiAgICByZWNpcGU6IFJlY2lwZTtcclxuICAgIHByaXZhdGUgX2VkaXRNb2RlID0nY3JlYXRlJztcclxuICAgIHByaXZhdGUgX3JlY2lwZUluZGV4OiBudW1iZXI7XHJcbiAgICBwcml2YXRlIF9zdWJtaXR0ZWQgPSBmYWxzZTtcclxuICAgICAgIFxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfcm91dGVQYXJhbXM6IFJvdXRlUGFyYW1zLCBwcml2YXRlIF9yb3V0ZXI6IFJvdXRlcixcclxuICAgIHByaXZhdGUgX3JlY2lwZXNTZXJ2aWNlOiBSZWNpcGVTZXJ2aWNlLCBwcml2YXRlIF9mb3JtQnVpbGRlcjogRm9ybUJ1aWxkZXIpe31cclxuICAgIFxyXG4gICAgb25BZGRJdGVtKGl0ZW1OYW1lOiBzdHJpbmcsIGl0ZW1BbW91bnQ6IHN0cmluZyl7XHJcbiAgICAgICAgKDxDb250cm9sQXJyYXk+dGhpcy5teUZvcm0uY29udHJvbHNbJ2luZ3JlZGllbnRzJ10pLnB1c2goXHJcbiAgICAgICAgICBuZXcgQ29udHJvbEdyb3VwKHtcclxuICAgICAgICAgICAgICAgIG5hbWU6IG5ldyBDb250cm9sKGl0ZW1OYW1lLCBWYWxpZGF0b3JzLnJlcXVpcmVkKSxcclxuICAgICAgICAgICAgICAgIGFtb3VudDogbmV3IENvbnRyb2woaXRlbUFtb3VudCwgVmFsaWRhdG9ycy5jb21wb3NlKFtcclxuICAgICAgICAgICAgICAgIFZhbGlkYXRvcnMucmVxdWlyZWQsIGhhc051bWJlcnMsIGdyZWF0ZXJaZXJvXHJcbiAgICAgICAgICAgICAgICBdKSlcclxuICAgICAgICAgIH0pICBcclxuICAgICAgICApO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICBvblJlbW92ZUl0ZW0oaW5kZXg6IG51bWJlcil7XHJcbiAgICAgICAgKDxDb250cm9sQXJyYXk+dGhpcy5teUZvcm0uY29udHJvbHNbJ2luZ3JlZGllbnRzJ10pLnJlbW92ZUF0KGluZGV4KTtcclxuICAgIH1cclxuICAgIFxyXG4gICAgb25TdWJtaXQoKXtcclxuICAgICAgICAvL3RoaXMgd29ya3MgYmVjYXVzZSBmb3JtIG1hdGNoZXMgcmVjaXBlIGNsYXNzXHJcbiAgICAgICAgdGhpcy5yZWNpcGUgPSB0aGlzLm15Rm9ybS52YWx1ZTtcclxuICAgICAgICBpZiAodGhpcy5fZWRpdE1vZGUgPT09ICdlZGl0Jyl7XHJcbiAgICAgICAgICAgIHRoaXMuX3JlY2lwZXNTZXJ2aWNlLnVwZGF0ZVJlY2lwZSh0aGlzLl9yZWNpcGVJbmRleCwgdGhpcy5yZWNpcGUpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3JlY2lwZXNTZXJ2aWNlLmluc2VydFJlY2lwZSh0aGlzLnJlY2lwZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuX3N1Ym1pdHRlZCA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5uYXZpZ2F0ZUJhY2soKTtcclxuICAgIH1cclxuICAgIFxyXG4gICAgb25DYW5jZWwoKXtcclxuICAgICAgICB0aGlzLm5hdmlnYXRlQmFjaygpO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICBwcml2YXRlIG5hdmlnYXRlQmFjaygpe1xyXG4gICAgICAgIHRoaXMuX3JvdXRlci5uYXZpZ2F0ZShbJ1JlY2lwZURldGFpbCcsIHtyZWNpcGVJbmRleDogdGhpcy5fcmVjaXBlSW5kZXh9XSk7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIG5nT25Jbml0KCl7XHJcbiAgICAgICAgdGhpcy5fZWRpdE1vZGUgPSB0aGlzLl9yb3V0ZVBhcmFtcy5nZXQoJ2VkaXRNb2RlJyk7XHJcbiAgICAgICAgbGV0IGZiUmVjaXBlTmFtZSA9ICcnO1xyXG4gICAgICAgIGxldCBmYlJlY2lwZUltYWdlVXJsID0gJyc7XHJcbiAgICAgICAgbGV0IGZiUmVjaXBlQ29udGVudCA9ICcnO1xyXG4gICAgICAgIGxldCBmYkluZ3JlZGllbnRzOiBDb250cm9sQXJyYXkgPSBuZXcgQ29udHJvbEFycmF5KFtdKTtcclxuICAgICAgICBcclxuICAgICAgICBpZiAodGhpcy5fZWRpdE1vZGUgPT09ICdlZGl0Jyl7XHJcbiAgICAgICAgICAgIHRoaXMuX3JlY2lwZUluZGV4ID0gK3RoaXMuX3JvdXRlUGFyYW1zLmdldCgnaXRlbUluZGV4Jyk7XHJcbiAgICAgICAgICAgIHRoaXMucmVjaXBlID0gdGhpcy5fcmVjaXBlc1NlcnZpY2UuZ2V0UmVjaXBlKHRoaXMuX3JlY2lwZUluZGV4KTtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIGZvciAoIGxldCBpID0gMDsgaSA8IHRoaXMucmVjaXBlLmluZ3JlZGllbnRzLmxlbmd0aDsgaSsrKXtcclxuICAgICAgICAgICAgICAgIGZiSW5ncmVkaWVudHMucHVzaChcclxuICAgICAgICAgICAgICAgICAgICBuZXcgQ29udHJvbEdyb3VwKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBuZXcgQ29udHJvbCh0aGlzLnJlY2lwZS5pbmdyZWRpZW50c1tpXS5uYW1lLCBWYWxpZGF0b3JzLnJlcXVpcmVkKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFtb3VudDogbmV3IENvbnRyb2wodGhpcy5yZWNpcGUuaW5ncmVkaWVudHNbaV0uYW1vdW50LCBWYWxpZGF0b3JzLmNvbXBvc2UoW1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgVmFsaWRhdG9ycy5yZXF1aXJlZCwgaGFzTnVtYmVycywgZ3JlYXRlclplcm9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0pKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgIGZiUmVjaXBlTmFtZSA9IHRoaXMucmVjaXBlLm5hbWU7XHJcbiAgICAgICAgICAgICAgICBmYlJlY2lwZUltYWdlVXJsID0gdGhpcy5yZWNpcGUuaW1hZ2VVcmw7XHJcbiAgICAgICAgICAgICAgICBmYlJlY2lwZUNvbnRlbnQgPSB0aGlzLnJlY2lwZS5jb250ZW50O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLm15Rm9ybSA9IHRoaXMuX2Zvcm1CdWlsZGVyLmdyb3VwKHtcclxuICAgICAgICAgICAgbmFtZTogW2ZiUmVjaXBlTmFtZSwgVmFsaWRhdG9ycy5yZXF1aXJlZF0sXHJcbiAgICAgICAgICAgIGltYWdlVXJsOiBbZmJSZWNpcGVJbWFnZVVybF0sXHJcbiAgICAgICAgICAgIGNvbnRlbnQ6IFtmYlJlY2lwZUNvbnRlbnQsIFZhbGlkYXRvcnMucmVxdWlyZWRdLFxyXG4gICAgICAgICAgICBpbmdyZWRpZW50czogdGhpcy5fZm9ybUJ1aWxkZXIuYXJyYXkoZmJJbmdyZWRpZW50cy5jb250cm9scylcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIHJvdXRlckNhbkRlYWN0aXZhdGUobmV4dEluc3RydWN0aW9uOiBDb21wb25lbnRJbnN0cnVjdGlvbiwgcHJldmlvdXNJbnN0cnVjdGlvbjogQ29tcG9uZW50SW5zdHJ1Y3Rpb24pe1xyXG4gICAgICAgIGlmICh0aGlzLl9zdWJtaXR0ZWQgfHwgdGhpcy5teUZvcm0ucHJpc3RpbmUpe1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGNvbmZpcm0oJ0RvIHlvdSB3YW50IHRvIGxlYXZlJyk7XHJcbiAgICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGhhc051bWJlcnMoY29udHJvbDogQ29udHJvbCk6e1tzOnN0cmluZ106IGJvb2xlYW59IHtcclxuICAgIGlmICghKCcnICsgY29udHJvbC52YWx1ZSkubWF0Y2goJ1xcXFxkKycpKXtcclxuICAgICAgICByZXR1cm4ge25vTnVtYmVyczogdHJ1ZX07XHJcbiAgICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGdyZWF0ZXJaZXJvKGNvbnRyb2w6IENvbnRyb2wpOntbczpzdHJpbmddOiBib29sZWFufSB7XHJcbiAgICBpZiAoISgrY29udHJvbC52YWx1ZSA+IDApKXtcclxuICAgICAgICByZXR1cm4ge3Rvb1NtYWxsOiB0cnVlfTtcclxuICAgIH1cclxufSJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
