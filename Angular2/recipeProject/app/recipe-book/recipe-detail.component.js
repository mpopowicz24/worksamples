System.register(['angular2/core', './recipe.service', 'angular2/router', '../shared/shopping-list.service'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, recipe_service_1, router_1, shopping_list_service_1;
    var RecipeDetailComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (recipe_service_1_1) {
                recipe_service_1 = recipe_service_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (shopping_list_service_1_1) {
                shopping_list_service_1 = shopping_list_service_1_1;
            }],
        execute: function() {
            RecipeDetailComponent = (function () {
                function RecipeDetailComponent(_routeParams, _router, _recipesService, _shoppingListService) {
                    this._routeParams = _routeParams;
                    this._router = _router;
                    this._recipesService = _recipesService;
                    this._shoppingListService = _shoppingListService;
                }
                RecipeDetailComponent.prototype.ngOnInit = function () {
                    //this.recipe = this._recipesService.getRecipe();
                    var itemIndex = this._routeParams.get('recipeIndex');
                    this._recipeIndex = itemIndex;
                    this.recipe = this._recipesService.getRecipe(itemIndex !== null ? +itemIndex : null);
                    console.log(itemIndex);
                };
                RecipeDetailComponent.prototype.onEdit = function () {
                    this._router.navigate(['RecipeEdit', { editMode: 'edit', itemIndex: this._recipeIndex }]);
                };
                RecipeDetailComponent.prototype.onDelete = function () {
                    this._recipesService.deleteRecipe(+this._recipeIndex);
                    this._router.navigate(['RecipeDetail']);
                };
                RecipeDetailComponent.prototype.onAddToShoppingList = function () {
                    this._shoppingListService.insertItems(this.recipe.ingredients);
                };
                RecipeDetailComponent = __decorate([
                    core_1.Component({
                        templateUrl: '../templates/recipes/recipe-detail.tpl.html',
                        providers: [shopping_list_service_1.ShoppingListService]
                    }), 
                    __metadata('design:paramtypes', [router_1.RouteParams, router_1.Router, recipe_service_1.RecipeService, shopping_list_service_1.ShoppingListService])
                ], RecipeDetailComponent);
                return RecipeDetailComponent;
            }());
            exports_1("RecipeDetailComponent", RecipeDetailComponent);
        }
    }
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInJlY2lwZS1ib29rL3JlY2lwZS1kZXRhaWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1lBV0E7Z0JBSUksK0JBQW9CLFlBQXlCLEVBQVUsT0FBZSxFQUM5RCxlQUE4QixFQUFVLG9CQUF5QztvQkFEckUsaUJBQVksR0FBWixZQUFZLENBQWE7b0JBQVUsWUFBTyxHQUFQLE9BQU8sQ0FBUTtvQkFDOUQsb0JBQWUsR0FBZixlQUFlLENBQWU7b0JBQVUseUJBQW9CLEdBQXBCLG9CQUFvQixDQUFxQjtnQkFBRSxDQUFDO2dCQUU1Rix3Q0FBUSxHQUFSO29CQUNJLGlEQUFpRDtvQkFDakQsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDLENBQUM7b0JBQ3JELElBQUksQ0FBQyxZQUFZLEdBQUcsU0FBUyxDQUFDO29CQUM5QixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFDLFNBQVMsS0FBSyxJQUFJLEdBQUcsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLENBQUM7b0JBQ3JGLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQzNCLENBQUM7Z0JBRUQsc0NBQU0sR0FBTjtvQkFDSSxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLFlBQVksRUFBRSxFQUFDLFFBQVEsRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUFFLElBQUksQ0FBQyxZQUFZLEVBQUMsQ0FBQyxDQUFDLENBQUE7Z0JBQzNGLENBQUM7Z0JBRUQsd0NBQVEsR0FBUjtvQkFDSSxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztvQkFDdEQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO2dCQUM1QyxDQUFDO2dCQUVELG1EQUFtQixHQUFuQjtvQkFDSSxJQUFJLENBQUMsb0JBQW9CLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ25FLENBQUM7Z0JBL0JMO29CQUFDLGdCQUFTLENBQUM7d0JBQ1AsV0FBVyxFQUFFLDZDQUE2Qzt3QkFDMUQsU0FBUyxFQUFFLENBQUMsMkNBQW1CLENBQUM7cUJBRW5DLENBQUM7O3lDQUFBO2dCQTRCRiw0QkFBQztZQUFELENBM0JBLEFBMkJDLElBQUE7WUEzQkQseURBMkJDLENBQUEiLCJmaWxlIjoicmVjaXBlLWJvb2svcmVjaXBlLWRldGFpbC5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgT25Jbml0fSBmcm9tICdhbmd1bGFyMi9jb3JlJztcclxuaW1wb3J0IHtSZWNpcGV9IGZyb20gJy4uL3NoYXJlZC9yZWNpcGUnXHJcbmltcG9ydCB7UmVjaXBlU2VydmljZX0gZnJvbSAnLi9yZWNpcGUuc2VydmljZSc7XHJcbmltcG9ydCB7Um91dGVQYXJhbXMsIFJvdXRlcn0gZnJvbSAnYW5ndWxhcjIvcm91dGVyJztcclxuaW1wb3J0IHtTaG9wcGluZ0xpc3RTZXJ2aWNlfSBmcm9tICcuLi9zaGFyZWQvc2hvcHBpbmctbGlzdC5zZXJ2aWNlJ1xyXG5cclxuQENvbXBvbmVudCh7ICAgIFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuLi90ZW1wbGF0ZXMvcmVjaXBlcy9yZWNpcGUtZGV0YWlsLnRwbC5odG1sJywgXHJcbiAgICBwcm92aWRlcnM6IFtTaG9wcGluZ0xpc3RTZXJ2aWNlXSAgICAgIFxyXG4gICAgICBcclxufSlcclxuZXhwb3J0IGNsYXNzIFJlY2lwZURldGFpbENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdHtcclxuICAgIHJlY2lwZTogUmVjaXBlO1xyXG4gICAgcHJpdmF0ZSBfcmVjaXBlSW5kZXg6IHN0cmluZztcclxuICAgICAgIFxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfcm91dGVQYXJhbXM6IFJvdXRlUGFyYW1zLCBwcml2YXRlIF9yb3V0ZXI6IFJvdXRlcixcclxuICAgIHByaXZhdGUgX3JlY2lwZXNTZXJ2aWNlOiBSZWNpcGVTZXJ2aWNlLCBwcml2YXRlIF9zaG9wcGluZ0xpc3RTZXJ2aWNlOiBTaG9wcGluZ0xpc3RTZXJ2aWNlKXt9XHJcbiAgICBcclxuICAgIG5nT25Jbml0KCl7XHJcbiAgICAgICAgLy90aGlzLnJlY2lwZSA9IHRoaXMuX3JlY2lwZXNTZXJ2aWNlLmdldFJlY2lwZSgpO1xyXG4gICAgICAgIGxldCBpdGVtSW5kZXggPSB0aGlzLl9yb3V0ZVBhcmFtcy5nZXQoJ3JlY2lwZUluZGV4Jyk7XHJcbiAgICAgICAgdGhpcy5fcmVjaXBlSW5kZXggPSBpdGVtSW5kZXg7ICAgICAgICBcclxuICAgICAgICB0aGlzLnJlY2lwZSA9IHRoaXMuX3JlY2lwZXNTZXJ2aWNlLmdldFJlY2lwZShpdGVtSW5kZXggIT09IG51bGwgPyAraXRlbUluZGV4IDogbnVsbCk7XHJcbiAgICAgICAgY29uc29sZS5sb2coaXRlbUluZGV4KTtcclxuICAgIH1cclxuICAgIFxyXG4gICAgb25FZGl0KCl7XHJcbiAgICAgICAgdGhpcy5fcm91dGVyLm5hdmlnYXRlKFsnUmVjaXBlRWRpdCcsIHtlZGl0TW9kZTogJ2VkaXQnLCBpdGVtSW5kZXg6IHRoaXMuX3JlY2lwZUluZGV4fV0pXHJcbiAgICB9XHJcbiAgICBcclxuICAgIG9uRGVsZXRlKCl7XHJcbiAgICAgICAgdGhpcy5fcmVjaXBlc1NlcnZpY2UuZGVsZXRlUmVjaXBlKCt0aGlzLl9yZWNpcGVJbmRleCk7XHJcbiAgICAgICAgdGhpcy5fcm91dGVyLm5hdmlnYXRlKFsnUmVjaXBlRGV0YWlsJ10pO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICBvbkFkZFRvU2hvcHBpbmdMaXN0KCl7XHJcbiAgICAgICAgdGhpcy5fc2hvcHBpbmdMaXN0U2VydmljZS5pbnNlcnRJdGVtcyh0aGlzLnJlY2lwZS5pbmdyZWRpZW50cyk7XHJcbiAgICB9XHJcbn0iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
