System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var Recipe;
    return {
        setters:[],
        execute: function() {
            Recipe = (function () {
                function Recipe(name, content, imageUrl, ingredients) {
                    this.name = name;
                    this.content = content;
                    this.imageUrl = imageUrl;
                    this.ingredients = ingredients;
                }
                return Recipe;
            }());
            exports_1("Recipe", Recipe);
        }
    }
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNoYXJlZC9yZWNpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7OztZQUVBO2dCQU1JLGdCQUFZLElBQVksRUFBRSxPQUFlLEVBQUUsUUFBZ0IsRUFBRSxXQUF5QjtvQkFDbkYsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7b0JBQ2pCLElBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO29CQUN2QixJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztvQkFDekIsSUFBSSxDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUE7Z0JBQ2pDLENBQUM7Z0JBQ0wsYUFBQztZQUFELENBWkEsQUFZQyxJQUFBO1lBWkQsMkJBWUMsQ0FBQSIsImZpbGUiOiJzaGFyZWQvcmVjaXBlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJbmdyZWRpZW50fSBmcm9tICcuL2luZ3JlZGllbnQnXHJcblxyXG5leHBvcnQgY2xhc3MgUmVjaXBlIHtcclxuICAgIG5hbWU6IHN0cmluZztcclxuICAgIGNvbnRlbnQ6IHN0cmluZztcclxuICAgIGltYWdlVXJsOiBzdHJpbmc7XHJcbiAgICBpbmdyZWRpZW50czogSW5ncmVkaWVudFtdO1xyXG4gICAgXHJcbiAgICBjb25zdHJ1Y3RvcihuYW1lOiBzdHJpbmcsIGNvbnRlbnQ6IHN0cmluZywgaW1hZ2VVcmw6IHN0cmluZywgaW5ncmVkaWVudHM6IEluZ3JlZGllbnRbXSl7XHJcbiAgICAgICB0aGlzLm5hbWUgPSBuYW1lO1xyXG4gICAgICAgdGhpcy5jb250ZW50ID0gY29udGVudDtcclxuICAgICAgIHRoaXMuaW1hZ2VVcmwgPSBpbWFnZVVybDtcclxuICAgICAgIHRoaXMuaW5ncmVkaWVudHMgPSBpbmdyZWRpZW50cyAgICAgICAgXHJcbiAgICB9XHJcbn0iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
