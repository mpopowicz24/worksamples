System.register(['angular2/core', '../shared/shopping-list.service'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, shopping_list_service_1;
    var ShoppingListEditComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (shopping_list_service_1_1) {
                shopping_list_service_1 = shopping_list_service_1_1;
            }],
        execute: function() {
            ShoppingListEditComponent = (function () {
                //don't need provider because it is in parent
                function ShoppingListEditComponent(_shoppingListService) {
                    this._shoppingListService = _shoppingListService;
                }
                ShoppingListEditComponent.prototype.onSubmit = function (item) {
                    this.ingredient !== null ? this._shoppingListService.updateItem(this._shoppingListService.getIndexOfItem(this.ingredient), item) : this._shoppingListService.insertItem(item);
                };
                ShoppingListEditComponent.prototype.onDelete = function () {
                    this._shoppingListService.deleteItem(this.ingredient);
                    this.ingredient = null;
                };
                ShoppingListEditComponent = __decorate([
                    core_1.Component({
                        selector: 'my-shopping-list-edit',
                        template: "\n        <h1>{{ingredient === null ? 'Add' : 'Edit'}} Item</h1>\n        <form id=\"shopping-list-add\" (ngSubmit)=\"onSubmit(f.value)\" #f=\"ngForm\">\n            <label for=\"item-name\">Name</label>\n            <input type=\"text\" id=\"item-name\" required [ngModel]=\"ingredient?.name\" ngControl=\"name\">\n            \n            <label for=\"item-amount\">Amount</label>\n            <input type=\"text\" id=\"item-amount\" required [ngModel]=\"ingredient?.amount\" ngControl=\"amount\">\n            <button type=\"submit\" class=\"btn\">{{ingredient === null ? 'Add' : 'Edit'}}</button>\n            <button class=\"btn danger\" *ngIf=\"ingredient !== null\" (click)=\"onDelete()\">Delete Item</button>\n       \n        </form>\n    ",
                        inputs: ['ingredient'],
                        styleUrls: ['src/css/shopping-list.css']
                    }), 
                    __metadata('design:paramtypes', [shopping_list_service_1.ShoppingListService])
                ], ShoppingListEditComponent);
                return ShoppingListEditComponent;
            }());
            exports_1("ShoppingListEditComponent", ShoppingListEditComponent);
        }
    }
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNob3BwaW5nLWxpc3Qvc2hvcHBpbmctbGlzdC1lZGl0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztZQXVCQTtnQkFHSSw2Q0FBNkM7Z0JBQzdDLG1DQUFvQixvQkFBeUM7b0JBQXpDLHlCQUFvQixHQUFwQixvQkFBb0IsQ0FBcUI7Z0JBQUUsQ0FBQztnQkFFaEUsNENBQVEsR0FBUixVQUFTLElBQWdCO29CQUNyQixJQUFJLENBQUMsVUFBVSxLQUFLLElBQUksR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2xMLENBQUM7Z0JBRUQsNENBQVEsR0FBUjtvQkFDRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztvQkFDdEQsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7Z0JBQzFCLENBQUM7Z0JBaENMO29CQUFDLGdCQUFTLENBQUM7d0JBQ1AsUUFBUSxFQUFFLHVCQUF1Qjt3QkFDakMsUUFBUSxFQUFFLCt1QkFZVDt3QkFDRCxNQUFNLEVBQUUsQ0FBQyxZQUFZLENBQUM7d0JBQ3RCLFNBQVMsRUFBRSxDQUFDLDJCQUEyQixDQUFDO3FCQUUzQyxDQUFDOzs2Q0FBQTtnQkFlRixnQ0FBQztZQUFELENBZEEsQUFjQyxJQUFBO1lBZEQsaUVBY0MsQ0FBQSIsImZpbGUiOiJzaG9wcGluZy1saXN0L3Nob3BwaW5nLWxpc3QtZWRpdC5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudH0gZnJvbSAnYW5ndWxhcjIvY29yZSc7XHJcbmltcG9ydCB7SW5ncmVkaWVudH0gZnJvbSAnLi4vc2hhcmVkL2luZ3JlZGllbnQnO1xyXG5pbXBvcnQge1Nob3BwaW5nTGlzdFNlcnZpY2V9IGZyb20gJy4uL3NoYXJlZC9zaG9wcGluZy1saXN0LnNlcnZpY2UnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ215LXNob3BwaW5nLWxpc3QtZWRpdCcsXHJcbiAgICB0ZW1wbGF0ZTogYFxyXG4gICAgICAgIDxoMT57e2luZ3JlZGllbnQgPT09IG51bGwgPyAnQWRkJyA6ICdFZGl0J319IEl0ZW08L2gxPlxyXG4gICAgICAgIDxmb3JtIGlkPVwic2hvcHBpbmctbGlzdC1hZGRcIiAobmdTdWJtaXQpPVwib25TdWJtaXQoZi52YWx1ZSlcIiAjZj1cIm5nRm9ybVwiPlxyXG4gICAgICAgICAgICA8bGFiZWwgZm9yPVwiaXRlbS1uYW1lXCI+TmFtZTwvbGFiZWw+XHJcbiAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIGlkPVwiaXRlbS1uYW1lXCIgcmVxdWlyZWQgW25nTW9kZWxdPVwiaW5ncmVkaWVudD8ubmFtZVwiIG5nQ29udHJvbD1cIm5hbWVcIj5cclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIDxsYWJlbCBmb3I9XCJpdGVtLWFtb3VudFwiPkFtb3VudDwvbGFiZWw+XHJcbiAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIGlkPVwiaXRlbS1hbW91bnRcIiByZXF1aXJlZCBbbmdNb2RlbF09XCJpbmdyZWRpZW50Py5hbW91bnRcIiBuZ0NvbnRyb2w9XCJhbW91bnRcIj5cclxuICAgICAgICAgICAgPGJ1dHRvbiB0eXBlPVwic3VibWl0XCIgY2xhc3M9XCJidG5cIj57e2luZ3JlZGllbnQgPT09IG51bGwgPyAnQWRkJyA6ICdFZGl0J319PC9idXR0b24+XHJcbiAgICAgICAgICAgIDxidXR0b24gY2xhc3M9XCJidG4gZGFuZ2VyXCIgKm5nSWY9XCJpbmdyZWRpZW50ICE9PSBudWxsXCIgKGNsaWNrKT1cIm9uRGVsZXRlKClcIj5EZWxldGUgSXRlbTwvYnV0dG9uPlxyXG4gICAgICAgXHJcbiAgICAgICAgPC9mb3JtPlxyXG4gICAgYCxcclxuICAgIGlucHV0czogWydpbmdyZWRpZW50J10sXHJcbiAgICBzdHlsZVVybHM6IFsnc3JjL2Nzcy9zaG9wcGluZy1saXN0LmNzcyddXHJcbiAgICBcclxufSlcclxuZXhwb3J0IGNsYXNzIFNob3BwaW5nTGlzdEVkaXRDb21wb25lbnQge1xyXG4gICAgaW5ncmVkaWVudDogSW5ncmVkaWVudDtcclxuICAgIFxyXG4gICAgLy9kb24ndCBuZWVkIHByb3ZpZGVyIGJlY2F1c2UgaXQgaXMgaW4gcGFyZW50XHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9zaG9wcGluZ0xpc3RTZXJ2aWNlOiBTaG9wcGluZ0xpc3RTZXJ2aWNlKXt9XHJcbiAgICBcclxuICAgIG9uU3VibWl0KGl0ZW06IEluZ3JlZGllbnQpe1xyXG4gICAgICAgIHRoaXMuaW5ncmVkaWVudCAhPT0gbnVsbCA/IHRoaXMuX3Nob3BwaW5nTGlzdFNlcnZpY2UudXBkYXRlSXRlbSh0aGlzLl9zaG9wcGluZ0xpc3RTZXJ2aWNlLmdldEluZGV4T2ZJdGVtKHRoaXMuaW5ncmVkaWVudCksIGl0ZW0pIDogdGhpcy5fc2hvcHBpbmdMaXN0U2VydmljZS5pbnNlcnRJdGVtKGl0ZW0pOyAgICAgXHJcbiAgICB9XHJcbiAgICBcclxuICAgIG9uRGVsZXRlKCl7XHJcbiAgICAgICB0aGlzLl9zaG9wcGluZ0xpc3RTZXJ2aWNlLmRlbGV0ZUl0ZW0odGhpcy5pbmdyZWRpZW50KTtcclxuICAgICAgIHRoaXMuaW5ncmVkaWVudCA9IG51bGw7XHJcbiAgICB9XHJcbn0iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
