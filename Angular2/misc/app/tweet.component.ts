import {Component, Input, Output, EventEmitter} from 'angular2/core';
import {TweetService} from './tweet.service';
import {LikeComponent} from './like.component';

@Component({
    selector: 'tweet',
    template: `
        <div *ngFor="#tweet of tweets">
            <div class="media" id="tweet">
                <div class="media-left">
                    <a href="#">
                    <img class="media-object" src="{{tweet.picture}}">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">
                        {{tweet.author}}
                        <span class="handle">{{tweet.handle}}</span>
                    </h4>
                    {{tweet.tweet}} <br/>
                    <like [iLike]=tweet.iLike [totalLikes]=tweet.totalLikes></like>
                </div>
            </div>
         </div>
    `,
    styles: [`
        .handle{
            color:#bbb;
        }
        img{
             border-radius: 10px;  
        }
        #tweet{
            margin:15px 15px;
            border:1px solid #ccc;
            border-radius: 5px;
            padding:5px;           
            
        }
    `],
    providers: [TweetService],
    directives: [LikeComponent]       
})
export class TweetComponent { 
    tweets;
    
    constructor(tweetService: TweetService){
        this.tweets = tweetService.getTweets();       
    }
    
}