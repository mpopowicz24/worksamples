import {Component} from 'angular2/core';
import {ChangePasswordFormComponent} from './change-password-form.component'
import {TweetComponent} from './tweet.component'
import {VoterComponent} from './voter.component'

@Component({
    selector: 'my-app',
    directives: [ChangePasswordFormComponent, TweetComponent, VoterComponent],
    template: `
        <change-password-form></change-password-form>
        
        <tweet></tweet>
        
        <voter></voter>
        
       
        
        
    `
})
export class AppComponent {
}