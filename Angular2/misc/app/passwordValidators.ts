import {Control, ControlGroup} from 'angular2/common'

export class PasswordValidators{
    
     static shouldBeUnique(control: Control){
         return new Promise((resolve, reject) => {
            setTimeout(function(){
                if (control.value == "mosh")
                    resolve({shouldBeUnique: true});
                 else
                    resolve(null);
            }, 1000); 
         });
     }
    
    
    static passwordsDontMatch(group: ControlGroup){
        var newPassword1 = group.find('newPassword1').value;
        var newPassword2 = group.find('newPassword2').value;
        
        // If either of these fields is empty, the validation 
        // will be bypassed. We expect the required validator to be 
        // applied first. 
        if (newPassword1 == '' || newPassword2 == '')
            return null;
        
        if (newPassword1 != newPassword2)
            return { passwordsDontMatch: true };
            
        return null;
       
       
       /*var newPassword1 = group.find('newPassword1');
        var newPassword2 = group.find('newPassword2');
                       
        if (newPassword1.value != newPassword2.value )
            return {passwordsDontMatch: true}
            
        return null;*/
    }
}