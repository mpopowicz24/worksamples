import {Component} from 'angular2/core';
import {ControlGroup, Control, Validators,FormBuilder} from 'angular2/common';
import {PasswordValidators} from './passwordValidators';

@Component({
    selector: 'change-password-form',
    templateUrl: 'app/change-password-form.component.html'
})
export class ChangePasswordFormComponent {
    form: ControlGroup;
    
    constructor(fb: FormBuilder){
        this.form = fb.group({
            currentPassword: ['', Validators.required],          
            newPassword1: ['', Validators.compose([
                Validators.required, 
                Validators.minLength(5)])],
            newPassword2: ['', Validators.required],
            
        }, { validator: PasswordValidators.passwordsDontMatch });
    }    
  
    changePassword(){
        // Validating the oldPassword on submit. In a real-world application
        // here, we'll use a service to call the server. The server would
        // tell us that the old password does not match. 
        var currentPassword = this.form.find('currentPassword');
        if (currentPassword.value != '1234') 
            currentPassword.setErrors({ validOldPassword: true });

        if (this.form.valid)
            alert("Password successfully changed.");
    }
    
    log(x){
        console.log(x);
    }
    
    signup(){      
        console.log(this)
    }
}