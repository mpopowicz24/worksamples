System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var PasswordValidators;
    return {
        setters:[],
        execute: function() {
            PasswordValidators = (function () {
                function PasswordValidators() {
                }
                PasswordValidators.shouldBeUnique = function (control) {
                    return new Promise(function (resolve, reject) {
                        setTimeout(function () {
                            if (control.value == "mosh")
                                resolve({ shouldBeUnique: true });
                            else
                                resolve(null);
                        }, 1000);
                    });
                };
                PasswordValidators.passwordsDontMatch = function (group) {
                    var newPassword1 = group.find('newPassword1').value;
                    var newPassword2 = group.find('newPassword2').value;
                    // If either of these fields is empty, the validation 
                    // will be bypassed. We expect the required validator to be 
                    // applied first. 
                    if (newPassword1 == '' || newPassword2 == '')
                        return null;
                    if (newPassword1 != newPassword2)
                        return { passwordsDontMatch: true };
                    return null;
                    /*var newPassword1 = group.find('newPassword1');
                     var newPassword2 = group.find('newPassword2');
                                    
                     if (newPassword1.value != newPassword2.value )
                         return {passwordsDontMatch: true}
                         
                     return null;*/
                };
                return PasswordValidators;
            }());
            exports_1("PasswordValidators", PasswordValidators);
        }
    }
});
//# sourceMappingURL=passwordValidators.js.map