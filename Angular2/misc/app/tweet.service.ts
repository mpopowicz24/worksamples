
export class TweetService{
    getTweets() {
        return [
            {author: "windward",
            picture: "http://lorempixel.com/100/100/business?image=1", 
            handle: "@windwardstudios",
            tweet: "Looking for a better company reporting or docgen app?",
            iLike: true,
            totalLikes: 1},                        
            {author: "Angular News", 
            picture: "http://lorempixel.com/100/100/people?image=2",
            handle: "@angular_news",
            tweet: "Right Relevance : Influencers, Articles and Conversations",
            iLike: true,
            totalLikes: 5},           
            {author: "UX & Bootstrap", 
            picture: "http://lorempixel.com/100/100/technics?image=3",
            handle: "@3rdwave",
            tweet: "10 reasons why web projects fail",
            iLike: false,
            totalLikes: 0}            
        ];
    }
}