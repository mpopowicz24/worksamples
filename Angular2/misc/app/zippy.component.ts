import {Component, Input, Output, EventEmitter} from 'angular2/core';

@Component({
    selector: 'zippy',
    template: `
        <div class="panel panel-default">
            <div class="panel-heading" (click)="toggle()">                
             {{title}}
             <i class="glyphicon pull-right"
                [ngClass]="{
                    'glyphicon-chevron-up': isExpanded,
                    'glyphicon-chevron-down': !isExpanded
                }">
            </i>                
            </div>
            <div class="panel-body" *ngIf="isExpanded">
                <ng-content></ng-content>
            </div>
        </div>
    
    `,
    styles: [`
        .panel{
            margin-bottom:0;
        }
        
    `]       
})
export class ZippyComponent { 
    isExpanded = false;
    @Input() title: string;
     
    toggle(){
        this.isExpanded = !this.isExpanded;           
    }    
}