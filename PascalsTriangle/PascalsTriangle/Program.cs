﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;

namespace PascalsTriangle
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("How many levels would you like? ");
            ulong levels = ulong.Parse(Console.ReadLine());

            for (ulong row = 0; row < levels; row++)
            {
                //spacing based on number of rows
                if (levels < 14)
                    Console.Write(new string(' ', (int)(levels - row) *2));
                else if (levels < 21)
                    Console.Write(new string(' ', (int)(levels - row) * 3));
                else if (levels < 26)
                    Console.Write(new string(' ', (int)(levels - row) * 4));
                else if (levels < 34)
                    Console.Write(new string(' ', (int)(levels - row) * 5));
                else
                    Console.Write(new string(' ', (int)(levels - row) * 6));


                for (ulong col = 0; col <= row; col++)
                {
                    double number = (GetFactorial(row)/(GetFactorial(col)*(GetFactorial(row - col))));

                    //spacing based on number of rows
                    if (levels < 14)
                        Console.Write($"{number, -4}");
                    else if (levels < 21)
                        Console.Write($"{number,-6}");
                    else if (levels < 26)
                        Console.Write($"{number,-8}");
                    else if (levels < 34)
                        Console.Write($"{number,-10}");
                    else
                        Console.Write($"{number,-12}");
                }
                Console.WriteLine();
            }

           Console.ReadLine();
        }

        static double GetFactorial(ulong number)
        {
            if (number == 0)
                return 1;

            return number*GetFactorial(number-1);
        }

    }
}
