# README #

Selected work samples from my time at The Software Guild and work on my own time.

FlooringProgram
 -C# Console Application for Flooring Retailer
 -Performs CRUD actions for orders
 -Used Factory Pattern and Interfaces to swap in-memory test and text file production repositories
 -Layered design using UI/BLL/Data/Models as well as a test layer
 -Used Migradoc library to make a simple PDF invoice

JustifyText
 -C# console application that accepts an array of words and prints out justified text

MazePathfinder
 -Solves a maze of 1's and 0's where 1's represent the paths. Option to see the maze being solved or just show the result. The mazes are stored in files, and they can also be entered via the console

FlowchartsAndPlanning
 -Sample flowcharts, UML, and database modelling.

SQL Scripts
 -Sample SQL scripts from labs

Angular2
 -Samples I created after learning from Udemy courses on Angular2