﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlooringProgram.Data.Interfaces
{
    public interface ILogRepository
    {
        void WriteToExceptionLog(Exception ex);        
    }
}

