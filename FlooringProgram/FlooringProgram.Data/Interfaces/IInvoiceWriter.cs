﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Models;
using MigraDoc.DocumentObjectModel;

namespace FlooringProgram.Data.Interfaces
{
    public interface IInvoiceWriter
    {
        void CreatePdfInvoice(Document document, Order order);
    }
}
