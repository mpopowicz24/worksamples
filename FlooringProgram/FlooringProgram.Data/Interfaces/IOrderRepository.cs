﻿using System;
using System.Collections.Generic;
using FlooringProgram.Models;

namespace FlooringProgram.Data.Interfaces
{
    public interface IOrderRepository
    {
        List<Order> LoadAllOrders(DateTime date);
        List<Order> LoadAllActiveOrdersInDateRange(DateTime startDate, DateTime endDate);
        List<Order> LoadAllActiveOrders(DateTime date);
        Order LoadOrder(DateTime date, int orderNumber);
        Order LoadActiveOrder(DateTime date, int orderNumber);
        Order AddNewOrder(Order order);
        Order UpdateOrder(Order order);
        void DeleteOrder(Order order);

    }
}
