﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Models;

namespace FlooringProgram.Data.Interfaces
{
    public interface IStateRepository
    {
        List<State> LoadAllStates();
        State LoadState(string state);
    }
}
