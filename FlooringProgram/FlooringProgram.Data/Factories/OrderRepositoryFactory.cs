﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using FlooringProgram.Data.Interfaces;
using FlooringProgram.Data.Repos;
using FlooringProgram.Data.TestRepos;
using Ninject.Modules;

namespace FlooringProgram.Data.Factories
{
    public class OrderRepositoryFactory
    {
        public static IOrderRepository GetOrderRepository()
        {
            IKernel kernel = new StandardKernel();

            var mode = ConfigurationManager.AppSettings["mode"];

            switch (mode)
            {
                case "test":
                    //kernel.Bind<IOrderRepository>().To<TestOrderRepository>();
                    //break;
                    return new TestOrderRepository();
                default:
                    //kernel.Bind<IOrderRepository>().To<TestOrderRepository>();
                    //break;
                    return new OrderRepository();

            }
           
        }
       
    }
}
