﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Data.Interfaces;
using FlooringProgram.Data.Repos;
using FlooringProgram.Data.TestRepos;

namespace FlooringProgram.Data.Factories
{
    public class InvoiceWriterFactory
    {
        public static IInvoiceWriter GetInvoiceWriter()
        {
            var mode = ConfigurationManager.AppSettings["mode"];

            switch (mode)
            {
                case "test":
                    return new TestPDFWriter();
                default:
                    return new PDFWriter();

            }
        }
    }
}
