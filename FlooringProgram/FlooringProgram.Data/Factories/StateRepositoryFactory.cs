﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Data.Interfaces;
using System.Configuration;
using FlooringProgram.Data.Repos;
using FlooringProgram.Data.TestRepos;

namespace FlooringProgram.Data.Factories
{
    public class StateRepositoryFactory
    {
        public static IStateRepository GetStateRepository()
        {
            var mode = ConfigurationManager.AppSettings["mode"];

            switch (mode)
            {
                case "test":
                    return new TestStateRepository();
                default:
                    return new StateRepository();

            }
        }
    }
}
