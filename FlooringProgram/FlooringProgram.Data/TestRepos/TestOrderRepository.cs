﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using FlooringProgram.Data.Interfaces;
using FlooringProgram.Models;

namespace FlooringProgram.Data.TestRepos
{
    public class TestOrderRepository : IOrderRepository
    {
        private static Dictionary<DateTime, List<Order>> _ordersDictionary;

        public TestOrderRepository()
        {
            if (_ordersDictionary == null)
            {
                _ordersDictionary = new Dictionary<DateTime, List<Order>>();

                _ordersDictionary[new DateTime(2016, 5, 16)] = new List<Order>()
                {
                    new Order() {OrderID=1, CustomerName="Mike's Construction",
                        StateInfo = new State() {StateAbbreviation="OH", StateName="Ohio", TaxRate=.075m },
                        ProductInfo = new Product() {ProductType="Maple Hardwood", CostPerSquareFoot=8.50m, LaborCostPerSquareFoot=5m },
                        Area=100, TotalMaterialCost=850m, TotalLaborCost=500m, Tax=101.25m, GrandTotal=1451.25m, OrderDate= DateTime.Parse("2016-5-16"), OrderStatus = OrderStatus.Active},
                    new Order() {OrderID=2, CustomerName="Mike's Remodeling",
                        StateInfo = new State() {StateAbbreviation="NY", StateName="New York", TaxRate=.08m },
                        ProductInfo = new Product() {ProductType="Italian Marble", CostPerSquareFoot=20m, LaborCostPerSquareFoot=8m },
                        Area=1000, TotalMaterialCost=20000m, TotalLaborCost=8000m, Tax=2240m, GrandTotal=30240, OrderDate= DateTime.Parse("2016-5-16"), OrderStatus = OrderStatus.Active}
                };
                _ordersDictionary[new DateTime(2016, 5, 12)] = new List<Order>()
                {
                     new Order() {OrderID=1, CustomerName="Mike's Flooring",
                        StateInfo = new State() {StateAbbreviation="IN", StateName="Indiana", TaxRate=.05m },
                        ProductInfo = new Product() {ProductType="Cheap Laminate", CostPerSquareFoot=4m, LaborCostPerSquareFoot=3m },
                        Area=100, TotalMaterialCost=400m, TotalLaborCost=300m, Tax=35m, GrandTotal=735m, OrderDate= DateTime.Parse("2016-5-12"), OrderStatus = OrderStatus.Active}
                };
            }
        }

        public List<Order> LoadAllOrders(DateTime date)
        {
            var orders = _ordersDictionary.Where(o => o.Key == date).Select(o => o.Value).FirstOrDefault();

            return orders;
        }

        public List<Order> LoadAllActiveOrdersInDateRange(DateTime startDate, DateTime endDate)
        {
            //throw new NotImplementedException();

            //loop through
            List<Order> orders = new List<Order>();

            while (startDate <= endDate)
            {
                if (_ordersDictionary.ContainsKey(startDate))
                {
                    orders.AddRange(_ordersDictionary[startDate].Where(o=>o.OrderStatus == OrderStatus.Active).ToList());
                }

                startDate = startDate.AddDays(1);
            }

            return orders;
        }

        public List<Order> LoadAllActiveOrders(DateTime date)
        {
            //throw new NotImplementedException(); //use to test logging

            var orders = _ordersDictionary.Where(o => o.Key == date).Select(o => o.Value).FirstOrDefault();

            if (orders == null)
                return new List<Order>();

            return orders.Where(x => x.OrderStatus == OrderStatus.Active).ToList();
        }

        public Order LoadOrder(DateTime date, int orderNumber)
        {
            var orders = LoadAllOrders(date);

            var order = orders.FirstOrDefault(o => o.OrderDate == date && o.OrderID == orderNumber);

            return order;
        }

        public Order LoadActiveOrder(DateTime date, int orderNumber)
        {
            var orders = LoadAllOrders(date);

            var order = orders.FirstOrDefault(o => o.OrderDate == date && o.OrderID == orderNumber && o.OrderStatus == OrderStatus.Active);

            return order;
        }

        public Order AddNewOrder(Order order)
        {
            var orders = LoadAllOrders(order.OrderDate);
            int nextID;

            if (orders == null)
                nextID = 1;
            else
                nextID = orders.Select(o => o.OrderID).Max() + 1;
            
            
            order.OrderStatus = OrderStatus.Active;

            //need to check if order exists

            if (_ordersDictionary.ContainsKey(order.OrderDate))
                _ordersDictionary[order.OrderDate].Add(order);
            else
            {
                //need to map each field to new object in the case of update to a different date
                //otherwise OrderStatus is set to delete on order object in delete method
                _ordersDictionary.Add(order.OrderDate, 
                    new List<Order>()
                    {
                        new Order()
                        {
                            CustomerName = order.CustomerName,
                            Area = order.Area,
                            GrandTotal = order.GrandTotal,
                            OrderDate = order.OrderDate,
                            OrderID = nextID,
                            OrderStatus = OrderStatus.Active,
                            ProductInfo = order.ProductInfo,
                            StateInfo = order.StateInfo,
                            Tax = order.Tax,
                            TotalLaborCost = order.TotalLaborCost,
                            TotalMaterialCost = order.TotalMaterialCost
                        }
                    });
            }
            return order;
        }

        public Order UpdateOrder(Order order)
        {
            var orders = LoadAllOrders(order.OrderDate);

            int orderIndex = orders.IndexOf(orders.FirstOrDefault(o => o.OrderID == order.OrderID));

            orders[orderIndex] = order;

            return order;
        }

        public void DeleteOrder(Order order)
        {
            var orders = LoadAllOrders(order.OrderDate);

            orders.FirstOrDefault(o => o.OrderID == order.OrderID).OrderStatus = OrderStatus.Deleted;
        }
    }
}
