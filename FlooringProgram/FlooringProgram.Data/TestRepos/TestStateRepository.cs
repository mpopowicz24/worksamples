﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Data.Interfaces;
using FlooringProgram.Models;

namespace FlooringProgram.Data.TestRepos
{
    class TestStateRepository : IStateRepository
    {
        private static List<State> _states;

        public TestStateRepository()
        {
            _states = new List<State>
            {
                 new State() {StateAbbreviation="IN", StateName="Indiana", TaxRate=.05m },
                 new State() {StateAbbreviation="OH", StateName="Ohio", TaxRate=.075m },
                 new State() {StateAbbreviation="NY", StateName="New York", TaxRate=.08m }
            };
        }

        public List<State> LoadAllStates()
        {
            return _states;
        }

        public State LoadState(string state)
        {
            return _states.FirstOrDefault(s => s.StateAbbreviation == state);
        }
    }
}
