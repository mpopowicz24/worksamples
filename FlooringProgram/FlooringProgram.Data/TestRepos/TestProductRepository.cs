﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Data.Interfaces;
using FlooringProgram.Models;

namespace FlooringProgram.Data.TestRepos
{
    public class TestProductRepository : IProductRepository
    {
        private static List<Product> _products;

        public TestProductRepository()
        {
            _products = new List<Product>
            {
                new Product() {ProductType = "Cheap Laminate", CostPerSquareFoot = 4m, LaborCostPerSquareFoot = 3m},
                new Product() {ProductType = "Maple Hardwood", CostPerSquareFoot = 8.50m, LaborCostPerSquareFoot = 5m},
                new Product() {ProductType = "Italian Marble", CostPerSquareFoot = 20m, LaborCostPerSquareFoot = 8m}
            };
        }

        public List<Product> LoadAllProducts()
        {
            return _products;
        }

        public Product LoadProduct(string productName)
        {
            return _products.FirstOrDefault(p=>p.ProductType == productName);
        }
    }
}
