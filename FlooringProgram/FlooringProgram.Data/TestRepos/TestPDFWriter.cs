﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Data.Interfaces;
using FlooringProgram.Models;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;

namespace FlooringProgram.Data.TestRepos
{
    public class TestPDFWriter : IInvoiceWriter
    {
        private const string _folder = "InvoiceFiles";

       public TestPDFWriter()
        {
            if (!Directory.Exists(_folder))
                Directory.CreateDirectory(_folder);
        }

        public void CreatePdfInvoice(Document document, Order order)
        {

            //add 20 to bottom padding of last table
            document.LastSection.LastTable.BottomPadding = 20;
            
            // Get the page size
            Unit width, height;
            PageSetup.GetPageSize(PageFormat.Letter, out width, out height);

            // Test Table
            Table testTable = new Table();
            testTable.Borders.Width = 1;
            var column = testTable.AddColumn(width);
            column.Format.Alignment = ParagraphAlignment.Center;

            Font font = new Font("Times New Roman", 36);

            // Add a row
            Row row = testTable.AddRow();
            row.BottomPadding = 10;
            row.TopPadding = 10;
            Cell cell = row.Cells[0];

            cell.Format.Font.Color = Colors.Red;
            cell.Format.Font.ApplyFont(font);
            cell.Borders.Top.Visible = true;
            cell.Borders.Left.Visible = false;
            cell.Borders.Right.Visible = false;
            cell.Borders.Bottom.Visible = true;

            cell.AddParagraph("Sample Invoice");
            cell.AddParagraph("For Testing Only!");

            document.LastSection.Add(testTable);

            // Create a renderer
            PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer();

            // Associate the MigraDoc document with a renderer
            pdfRenderer.Document = document;

            // Layout and render document to PDF
            pdfRenderer.RenderDocument();

            // Save and show the document
            string filepath = _folder + "\\testInvoice.pdf";
           
            pdfRenderer.PdfDocument.Save(filepath);
            Process.Start(filepath);
        }
    }
}
