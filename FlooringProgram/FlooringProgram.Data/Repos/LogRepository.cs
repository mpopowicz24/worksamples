﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Data.Interfaces;

namespace FlooringProgram.Data.Repos
{
    public class LogRepository : ILogRepository
    {
        private const string _Folder = "LogFiles";

        public LogRepository()
        {
            if (!Directory.Exists(_Folder))            
                Directory.CreateDirectory(_Folder);            
        }

        public void WriteToExceptionLog(Exception ex)
        {
            string filePath = _Folder + "\\exceptions.txt";

            StackTrace st = new StackTrace(ex, true);

            string stackString = "";

            foreach (var frame in st.GetFrames())
            {
                stackString += "file: "+frame.GetFileName()+"/"+frame.GetMethod()+"/line: "+frame.GetFileLineNumber()+"//*";
            }
            
            using (StreamWriter sw = File.AppendText(filePath))
            {
                sw.WriteLine($"{ex.Message}, {stackString}, {DateTime.Now.ToString("G")}");
            }
        }
                
    }
}
