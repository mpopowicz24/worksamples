﻿using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using FlooringProgram.Data.Interfaces;
using FlooringProgram.Models;
using Microsoft.VisualBasic.FileIO;

namespace FlooringProgram.Data.Repos
{
    public class OrderRepository : IOrderRepository
    {
        private const string _Folder = "OrderFiles";

        public OrderRepository()
        {
            if (!Directory.Exists(_Folder))
                Directory.CreateDirectory(_Folder);
        }

        public List<Order> LoadAllOrders(DateTime date)
        {
            string filepath = CreateFilePathFromDate(date);

            if (!File.Exists(filepath))
            {
                return null;
            }

            List<Order> results = new List<Order>();

            var rows = File.ReadAllLines(filepath);

            for (int i = 1; i < rows.Length; i++)
            {
                Order order = CreateOrderFromRow(rows[i], date);
                results.Add(order);
            }

            return results;
        }

        public List<Order> LoadAllActiveOrdersInDateRange(DateTime startDate, DateTime endDate)
        {

            List<Order> results = new List<Order>();

            while (startDate <= endDate)
            {
                string filepath = CreateFilePathFromDate(startDate);

                if (!File.Exists(filepath))
                {
                    startDate = startDate.AddDays(1);
                    continue;
                }

                var rows = File.ReadAllLines(filepath);

                for (int i = 1; i < rows.Length; i++)
                {
                    Order order = CreateOrderFromRow(rows[i], startDate);
                    results.Add(order);
                }
                startDate = startDate.AddDays(1);
            }

            return results.Where(o=>o.OrderStatus == OrderStatus.Active).ToList();
        }

        public List<Order> LoadAllActiveOrders(DateTime date)
        {
            //throw new NotImplementedException(); //use to test logging

            List<Order> orders = LoadAllOrders(date);

            if (orders == null)
                return new List<Order>();

            return orders.Where(o => o.OrderStatus == OrderStatus.Active).ToList();
        }

        public Order LoadOrder(DateTime date, int orderNumber)
        {
            List<Order> orders = LoadAllOrders(date);

            return orders.FirstOrDefault(o => o.OrderDate == date && o.OrderID == orderNumber);
        }

        public Order LoadActiveOrder(DateTime date, int orderNumber)
        {
            List<Order> orders = LoadAllOrders(date);

            return orders.FirstOrDefault(o => o.OrderDate == date && o.OrderID == orderNumber && o.OrderStatus == OrderStatus.Active);
        }

        public Order AddNewOrder(Order order)
        {
            List<Order> orders = LoadAllOrders(order.OrderDate);

            Order newOrder = new Order()
            {
                CustomerName = order.CustomerName,
                Area = order.Area,
                GrandTotal = order.GrandTotal,
                OrderDate = order.OrderDate,
                OrderStatus = OrderStatus.Active,
                ProductInfo = order.ProductInfo,
                StateInfo = order.StateInfo,
                Tax = order.Tax,
                TotalLaborCost = order.TotalLaborCost,
                TotalMaterialCost = order.TotalMaterialCost
                
            };

            if (orders == null)
            {
                orders = new List<Order>();
                newOrder.OrderID = 1;
            }
            else
            {
                newOrder.OrderID = orders.Select(o => o.OrderID).Max() + 1;
            }
                        
            orders.Add(newOrder);

            string filePath = CreateFilePathFromDate(order.OrderDate);
            OverwriteFile(orders, filePath);

            return newOrder;
        }

        public Order UpdateOrder(Order order)
        {
            List<Order> orders = LoadAllOrders(order.OrderDate);

            int orderIndex = orders.IndexOf(orders.FirstOrDefault(o => o.OrderID == order.OrderID));

            orders[orderIndex] = order;

            string filepath = CreateFilePathFromDate(order.OrderDate);
            OverwriteFile(orders, filepath);

            return order;
        }

        public void DeleteOrder(Order order)
        {
            string filepath = CreateFilePathFromDate(order.OrderDate);

            List<Order> orders = LoadAllOrders(order.OrderDate);

            orders.FirstOrDefault(o => o.OrderID == order.OrderID).OrderStatus = OrderStatus.Deleted;

            OverwriteFile(orders, filepath);
        }

        private void OverwriteFile(List<Order> orders, string filePath)
        {
            string header = "OrderID, CustomerName, State, TaxRate, ProductType, Area, CostPerSquareFoot, LaborPerSquareFoot, TotalMaterialCost, TotalLaborCost, Tax, GrandTotal, Status";

            File.Delete(filePath);

            
            
            using (var writer = File.CreateText(filePath))
            {
                writer.WriteLine(header);
                foreach (var order in orders)
                {
                    //Encode customer name and product type inside " " if there are commas
                    string customerName = string.Format($"\"{order.CustomerName}\"");
                    string productType = string.Format($"\"{order.ProductInfo.ProductType}\"");

                    writer.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12}",
                        order.OrderID,
                        customerName,
                        order.StateInfo.StateAbbreviation,
                        order.StateInfo.TaxRate,
                        productType,
                        order.Area, 
                        order.ProductInfo.CostPerSquareFoot,
                        order.ProductInfo.LaborCostPerSquareFoot,
                        order.TotalMaterialCost,
                        order.TotalLaborCost,
                        order.Tax,
                        order.GrandTotal,
                        (int)order.OrderStatus
                    );
                }
            }
        }

        private string CreateFilePathFromDate(DateTime date)
        {
            return string.Format(_Folder + "\\Orders_" + date.ToString("MMddyyyy") + ".txt");
        }

        private Order CreateOrderFromRow(string row, DateTime currentDate)
        {
            TextFieldParser parser = new TextFieldParser(new StringReader(row));
            parser.HasFieldsEnclosedInQuotes = true;
            parser.SetDelimiters(",");
            var columns = parser.ReadFields();

            var order = new Order()
            {
                OrderID = int.Parse(columns[0]),
                CustomerName = columns[1],
                StateInfo = new State() { StateAbbreviation = columns[2], TaxRate = decimal.Parse(columns[3]) },
                ProductInfo = new Product() { ProductType = columns[4], CostPerSquareFoot = decimal.Parse(columns[6]), LaborCostPerSquareFoot = decimal.Parse(columns[7]) },
                Area = decimal.Parse(columns[5]),
                TotalMaterialCost = decimal.Parse(columns[8]),
                TotalLaborCost = decimal.Parse(columns[9]),
                Tax = decimal.Parse(columns[10]),
                GrandTotal = decimal.Parse(columns[11]),
                OrderStatus = (OrderStatus)int.Parse(columns[12]),
                OrderDate = currentDate
            };

            //add state name to model
            StateRepository stateRepo = new StateRepository();
            order.StateInfo.StateName = stateRepo.LoadState(order.StateInfo.StateAbbreviation).StateName;

            return order;
        }
    }
}
