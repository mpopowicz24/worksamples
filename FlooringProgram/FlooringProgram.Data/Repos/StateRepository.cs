﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Data.Interfaces;
using FlooringProgram.Models;

namespace FlooringProgram.Data.Repos
{
    public class StateRepository: IStateRepository
    {
        private const string _filePath = @"DataFiles\Taxes.txt";


        public List<State> LoadAllStates()
        {
            List<State> results = new List<State>();

            string[] rows = File.ReadAllLines(_filePath);

            for (int i = 1; i < rows.Length; i++)
            {
                string[] columns = rows[i].Split(',');

                State state = new State()
                {
                    StateAbbreviation = columns[0],
                    StateName = columns[1],
                    TaxRate = (decimal.Parse(columns[2])/100)
                };
                
                results.Add(state);
            }

            return results;
        }

        public State LoadState(string state)
        {
            List<State> states = LoadAllStates();

            return states.FirstOrDefault(s => s.StateAbbreviation == state);
        }
    }
}
