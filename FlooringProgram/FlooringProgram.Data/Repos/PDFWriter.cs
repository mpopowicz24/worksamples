﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Data.Interfaces;
using FlooringProgram.Models;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;

namespace FlooringProgram.Data.Repos
{
    public class PDFWriter : IInvoiceWriter
    {

        private const string _folder = "InvoiceFiles";
        
        public PDFWriter()
        {
            if (!Directory.Exists(_folder))
                Directory.CreateDirectory(_folder);
        }

        public void CreatePdfInvoice(Document document, Order order)
        {
            // Create a renderer
            PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer();

            // Associate the MigraDoc document with a renderer
            pdfRenderer.Document = document;

            // Layout and render document to PDF
            pdfRenderer.RenderDocument();

            // Save and show the document
            string filepath = CreateFilePathFromOrder(order);
            
            pdfRenderer.PdfDocument.Save(filepath);
            Process.Start(filepath);
        }

        private string CreateFilePathFromOrder(Order order)
        {
            return string.Format(_folder + "\\Invoice_" + order.OrderDate.ToString("MMddyyyy") + "_" + order.OrderID +".pdf");
        }
    }
}
