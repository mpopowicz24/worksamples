﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Data.Interfaces;
using FlooringProgram.Models;

namespace FlooringProgram.Data.Repos
{
    public class ProductRepository : IProductRepository
    {
        private const string _filePath = @"DataFiles\Products.txt";

        public List<Product> LoadAllProducts()
        {
            List<Product> results = new List<Product>();

            string[] rows = File.ReadAllLines(_filePath);

            for (int i = 1; i < rows.Length; i++)
            {
                string[] columns = rows[i].Split(',');

                var product = new Product()
                {
                    ProductType = columns[0],
                    CostPerSquareFoot = decimal.Parse(columns[1]),
                    LaborCostPerSquareFoot = decimal.Parse(columns[2])
                };
                
                results.Add(product);
            }

            return results;
        }

        public Product LoadProduct(string productName)
        {
            List<Product> products = LoadAllProducts();
            return products.FirstOrDefault(s => s.ProductType == productName);
        }
    }
}
