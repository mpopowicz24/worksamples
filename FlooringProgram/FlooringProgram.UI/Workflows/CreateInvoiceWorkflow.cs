﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.BLL;
using FlooringProgram.Models;
using FlooringProgram.UI.Utilities;

namespace FlooringProgram.UI.Workflows
{
    public class CreateInvoiceWorkflow
    {
        public void Execute() 
        {
            Console.Clear();
            DisplayScreens.DisplayHeader("Create Invoice", 20);
            OrderManager orderManager = new OrderManager();
            DateTime date = UserPrompts.GetDateFromUser("Enter date of order: ");

            var ordersResponse = orderManager.GetAllActiveOrders(date);

            if (!ordersResponse.Success)
            {
                DisplayScreens.InlineError(ordersResponse.Message);
                UserPrompts.PressKeyForContinue();
                return;
            }

            Console.Clear();
            Console.WriteLine($"Orders for: {date.ToString("d")}\n");
            Order order = UserPrompts.GetOrderFromUser("Enter order number to create invoice: ", ordersResponse.Data);

            OrderManager manager = new OrderManager();
            manager.CreatePDF(order);
        }

    }
}
