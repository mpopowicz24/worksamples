﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.BLL;
using FlooringProgram.Models;
using FlooringProgram.UI.Utilities;

namespace FlooringProgram.UI.Workflows
{
    public class EditOrderWorkflow
    {
        private Order _order;

        public EditOrderWorkflow() { }

        public EditOrderWorkflow(Order order)
        {
            _order = order;
        }

        public void Execute()
        {
            OrderManager orderManager = new OrderManager();
            StateManager stateManager = new StateManager();
            ProductManager productManager = new ProductManager();

            if (_order == null)
            {
                Console.Clear();
                DisplayScreens.DisplayHeader("Edit an Order", 40);
                DateTime date = UserPrompts.GetDateFromUser("Enter date of order: ");

                var ordersResponse = orderManager.GetAllActiveOrders(date);

                if (!ordersResponse.Success)
                {
                    DisplayScreens.InlineError(ordersResponse.Message);
                    UserPrompts.PressKeyForContinue();
                    return;
                }

                Console.Clear();
                Console.WriteLine($"Orders for: {date.ToString("d")}\n");
                _order = UserPrompts.GetOrderFromUser("Enter order number to edit: ", ordersResponse.Data);
            }


            Console.Clear();
            DisplayScreens.DisplayOrderDetails(_order);

            DisplayScreens.InlineError("\nEnter new values or leave field blank to remain unchanged.");

            string name = UserPrompts.GetStringFromUser($"\nEnter Customer Name ({_order.CustomerName}): ");
            if (name != "")
                _order.CustomerName = name;
            
            var stateResponse = stateManager.GetAllStates();

            if (!stateResponse.Success)
            {
                DisplayScreens.WorkflowErrorScreen(stateResponse.Message);
                return;
            }
            Console.WriteLine();
            State state = UserPrompts.GetStateFromUser("Enter State: ", stateResponse.Data, true);

            if (state != null)
                _order.StateInfo = state;
            
            var productsResponse = productManager.GetAllProducts();

            if (!productsResponse.Success)
            {
                DisplayScreens.WorkflowErrorScreen(productsResponse.Message);
                return;
            }

            var newProduct = UserPrompts.GetProductFromUser("Enter a product number: ", productsResponse.Data, true);

            if (newProduct != null)
                _order.ProductInfo = newProduct;

            decimal area = UserPrompts.GetDecimalFromUser($"Enter area in square feet ({_order.Area}): ", true);

            if (area != 0)
                _order.Area = area;

            DateTime? newDate = UserPrompts.GetNullableDateFromUser($"Enter order date ({_order.OrderDate.ToString("d")}): ", true);

            orderManager.CalculateOrderDetails(_order);

            Response<Order> updateResponse;

            if (newDate == null)
                updateResponse = orderManager.UpdateOrder(_order);
            else
                updateResponse = orderManager.UpdateOrder(_order, newDate);
            

            if (updateResponse.Success)
            {
                Console.Clear();
                DisplayScreens.DisplayOrderDetails(updateResponse.Data);
                Console.WriteLine();
                UserPrompts.PressKeyForContinue();
            }
            else
            {
                DisplayScreens.WorkflowErrorScreen(updateResponse.Message);
                UserPrompts.PressKeyForContinue();
            }
        }
    }
}
