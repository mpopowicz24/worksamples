﻿using FlooringProgram.UI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.BLL;
using FlooringProgram.Models;

namespace FlooringProgram.UI.Workflows
{
    public class AddOrderWorkflow
    {
        public void Execute()
        {
            Order newOrder = new Order();
            StateManager stateManager = new StateManager();
            ProductManager productManager = new ProductManager();
            OrderManager orderManager = new OrderManager();

            Console.Clear();
            DisplayScreens.DisplayHeader("Enter a new order", 40);

            newOrder.CustomerName = UserPrompts.GetStringFromUser("\nCustomer Name: ");

            var stateResponse = stateManager.GetAllStates();
            
            if (!stateResponse.Success)
            {
                DisplayScreens.WorkflowErrorScreen(stateResponse.Message);
                return;
            }
            Console.WriteLine();
            newOrder.StateInfo = UserPrompts.GetStateFromUser("Enter State: ", stateResponse.Data);
            newOrder.OrderDate = UserPrompts.GetDateFromUser("Order Date: ");
            
            
            var productsResponse = productManager.GetAllProducts();

            if (!productsResponse.Success)
            {
                DisplayScreens.WorkflowErrorScreen(productsResponse.Message);
                return;
            }

            Console.WriteLine();
            newOrder.ProductInfo = UserPrompts.GetProductFromUser("Enter a product number: ", productsResponse.Data);

            newOrder.Area = UserPrompts.GetDecimalFromUser("Enter area in square feet: ");
           
            newOrder = orderManager.CalculateOrderDetails(newOrder);

            Console.Clear();
            DisplayScreens.DisplayOrderDetails(newOrder);

            bool commit = UserPrompts.ConfirmOrQuit("\nDo you want to complete this order (y/n)? ");

            if (commit)
            {
                var orderResponse = orderManager.AddNewOrder(newOrder);

                if (orderResponse.Success)
                {
                    Console.Clear();
                    DisplayScreens.DisplayOrderDetails(orderResponse.Data);
                    Console.WriteLine();
                    UserPrompts.PressKeyForContinue();
                }
                else
                {
                    DisplayScreens.WorkflowErrorScreen(stateResponse.Message);
                }
            }
        }
    }
}
