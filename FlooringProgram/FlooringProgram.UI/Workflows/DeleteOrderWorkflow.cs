﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.BLL;
using FlooringProgram.Models;
using FlooringProgram.UI.Utilities;

namespace FlooringProgram.UI.Workflows
{
    public class DeleteOrderWorkflow
    {
        private Order _order;

        public DeleteOrderWorkflow(){ }

        public DeleteOrderWorkflow(Order order)
        {
            _order = order;
        }

        public void Execute()
        {
            OrderManager orderManager = new OrderManager();

            if (_order == null)
            {
                Console.Clear();
                DisplayScreens.DisplayHeader("Delete an Order", 40);
                DateTime date = UserPrompts.GetDateFromUser("Enter date of order: ");

                var ordersResponse = orderManager.GetAllActiveOrders(date);

                if (!ordersResponse.Success)
                {
                    DisplayScreens.InlineError(ordersResponse.Message);
                    UserPrompts.PressKeyForContinue();
                    return;
                }

                Console.Clear();
                Console.WriteLine($"Orders for: {date.ToString("d")}\n");
                _order = UserPrompts.GetOrderFromUser("Enter order number: ", ordersResponse.Data);
            }

            Console.Clear();
            DisplayScreens.DisplayOrderDetails(_order);

            bool confirmDelete = UserPrompts.ConfirmOrQuit("\nAre you sure you want to delete this order (y/n)? ");

            if (confirmDelete)
            {
                var deleteResponse = orderManager.DeleteOrder(_order);

                if (deleteResponse.Success)
                {
                    DisplayScreens.InlineError("Order Deleted");
                    UserPrompts.PressKeyForContinue();
                }
                else
                {
                    Console.WriteLine(deleteResponse.Message);
                    UserPrompts.PressKeyForContinue();
                }
            }      
        }
    }
}
