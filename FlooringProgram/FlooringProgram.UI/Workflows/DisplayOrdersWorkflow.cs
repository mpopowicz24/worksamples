﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.UI.Utilities;
using FlooringProgram.BLL;
using System.Threading;

namespace FlooringProgram.UI.Workflows
{
    public class DisplayOrdersWorkflow
    {
        public void Execute()
        {                   
            Console.Clear();
            DateTime date = UserPrompts.GetDateFromUser("Enter a date to lookup (mm-dd-yyyy): ");

            OrderManager manager = new OrderManager();
            var response = manager.GetAllActiveOrders(date);

            if (response.Success)
            {
                Console.Clear();
                Console.WriteLine($"Orders for: {date.ToString("d")}\n");
                var order = UserPrompts.GetOrderFromUser("Enter order number to view details or press enter to go back: ", response.Data, true);
                if (order != null)
                {
                    DisplayOrderWorkflow displayOrderWorkflow = new DisplayOrderWorkflow();
                    displayOrderWorkflow.Execute(order);
                }
            }
            else
            {
                DisplayScreens.InlineError(response.Message);
                UserPrompts.PressKeyForContinue();
            }
                
             
        }
    }
}
