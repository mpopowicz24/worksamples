﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Models;
using FlooringProgram.UI.Utilities;

namespace FlooringProgram.UI.Workflows
{
    class DisplayOrderWorkflow
    {
        public void Execute(Order order)
        {
            Console.Clear();
            DisplayScreens.DisplayOrderDetails(order);
            Console.WriteLine();
            char input = UserPrompts.UpdateOrDeletePrompt("Enter (u)pdate, (d)elete or leave blank to go back: ");

            switch (input)
            {
                case 'U':
                    EditOrderWorkflow editOrderWorkflow = new EditOrderWorkflow(order);
                    editOrderWorkflow.Execute();
                    break;
                case 'D':
                    DeleteOrderWorkflow deleteOrderWorkflow = new DeleteOrderWorkflow(order);
                    deleteOrderWorkflow.Execute();
                    break;
            }
        }
    }
}
