﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.UI.Utilities;

namespace FlooringProgram.UI.Workflows
{
    class ReportMenuWorkflow
    {
        public void Execute()
        {
            do
            {
                Console.Clear();
                DisplayScreens.DisplayHeader("Reports", 40);
                Console.WriteLine("1. Show all orders in last 30 days");
                Console.WriteLine("2. Display state and tax info");
                Console.WriteLine("3. Display product info");
                Console.WriteLine("4. Back to main menu");

                string input = UserPrompts.GetStringFromUser("\nEnter Choice: ");

                if (input == "4")
                    break;

                ProcessChoice(input);

            } while (true);

        }

        private void ProcessChoice(string choice)
        {
            switch (choice)
            {
                
                case "1":
                    Console.Clear();
                    ReportScreens.DisplayLastThirtyDayOrderReport();
                    break;
                case "2":
                    Console.Clear();
                    ReportScreens.DisplayStatesReport();
                    break;
                case "3":
                    Console.Clear();
                    ReportScreens.DisplayProductsReport();
                    break;
            }
        }
    }
}
