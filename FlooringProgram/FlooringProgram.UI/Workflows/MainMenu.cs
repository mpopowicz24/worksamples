﻿using FlooringProgram.UI.Utilities;
using FlooringProgram.UI.Workflows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using FlooringProgram.BLL;
using FlooringProgram.Models;

namespace FlooringProgram.UI.Workflows
{
    class MainMenu
    {
        public void Execute()
        {
            do
            {
                Console.Clear();
                if (ConfigurationManager.AppSettings["mode"] == "test")
                    DisplayScreens.DisplayHeader("SG Flooring Corporation - Test Mode", 40, ConsoleColor.Red);
                else
                    DisplayScreens.DisplayHeader("SG Flooring Corporation", 30);                
                Console.WriteLine("1. Display Orders");
                Console.WriteLine("2. Add an Order");
                Console.WriteLine("3. Edit an Order");
                Console.WriteLine("4. Remove an Order");
                Console.WriteLine("5. Reports");
                Console.WriteLine("6. Create Invoice");
                Console.WriteLine("7. Quit");                

                string input = UserPrompts.GetStringFromUser("\nEnter Choice: ");

                if (input == "7")
                    break;

                ProcessChoice(input);

            } while (true);
        }

        private void ProcessChoice(string choice)
        {
            switch (choice)
            {
                case "1":
                    DisplayOrdersWorkflow displayOrdersWF = new DisplayOrdersWorkflow();
                    displayOrdersWF.Execute();
                    break;
                case "2":
                    AddOrderWorkflow addOrderWorkflow = new AddOrderWorkflow();
                    addOrderWorkflow.Execute();
                    break;
                case "3":
                    EditOrderWorkflow editOrderWorkflow = new EditOrderWorkflow();
                    editOrderWorkflow.Execute();
                    break;
                case "4":
                    DeleteOrderWorkflow deleteOrderWorkflow = new DeleteOrderWorkflow();
                    deleteOrderWorkflow.Execute();
                    break;
                case "5":
                    ReportMenuWorkflow reportMenuWorkflow = new ReportMenuWorkflow();
                    reportMenuWorkflow.Execute();
                    break;
                case "6":
                    CreateInvoiceWorkflow createInvoiceWorkflow = new CreateInvoiceWorkflow();
                    createInvoiceWorkflow.Execute();
                    break;
            }
        }
    }
}
