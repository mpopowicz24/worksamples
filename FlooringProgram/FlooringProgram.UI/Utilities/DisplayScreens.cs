﻿using FlooringProgram.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlooringProgram.UI.Utilities
{
    class DisplayScreens
    {
        public static void WorkflowErrorScreen(string message)
        {
            Console.Clear();
            Console.WriteLine("An error occured. {0}", message);
            UserPrompts.PressKeyForContinue();
        }
        
        public static void DisplayHeader(string message, int length, ConsoleColor color = ConsoleColor.Gray)
        {
            int padding = (length - message.Length)/2;

            Console.ForegroundColor = color;
            Console.WriteLine(new string (' ', padding) + message);
            Console.WriteLine(new string('-', length));
            Console.ForegroundColor = ConsoleColor.Gray;
        }  

        public static void DisplayListOfOrders(List<Order> orders)
        {
            Console.WriteLine($"{"ID", -4}{"Name",-30}{"State",-6}{"Product",-20}{"Area",-6}{"Grand Total",12}");
            Console.WriteLine(new string('-', 85));

            foreach (var order in orders)
            {
                Console.WriteLine($"{order.OrderID,-4}{order.CustomerName,-30}{order.StateInfo.StateAbbreviation,-6}" +
                    $"{order.ProductInfo.ProductType, -20}{order.Area,-6}{order.GrandTotal,12:c}");
            }

            Console.WriteLine();           
        }

        public static void DisplayListOfOrdersGroupByDate(List<Order> orders)
        {
            var groupedOrders = orders.GroupBy(o => o.OrderDate);

            foreach (var groupedOrder in groupedOrders)
            {
                Console.WriteLine("Date: " + groupedOrder.Key.ToString("d"));
                Console.WriteLine($"{"ID",-4}{"Name",-30}{"State",-6}{"Product",-20}{"Area",-6}{"Grand Total",12}");
                Console.WriteLine(new string('-', 85));

                foreach (var order in groupedOrder)
                {
                    Console.WriteLine($"{order.OrderID,-4}{order.CustomerName,-30}{order.StateInfo.StateAbbreviation,-6}" +
                   $"{order.ProductInfo.ProductType,-20}{order.Area,-6}{order.GrandTotal,12:c}");
                }
                Console.WriteLine("\n");
            }
    
            Console.WriteLine();
        }

        public static void DisplayListOfProducts(List<Product> products)
        {
            DisplayHeader($"{" Product", -15}{"$ per Sq/Ft", 11}", 26);

            int i = 1;
            foreach (var product in products)
            {
                Console.WriteLine($" {i}. {product.ProductType, -11}{product.LaborCostPerSquareFoot+product.CostPerSquareFoot, 11:c}");
                i++;
            }
        }

        public static void DisplayListOfStates(List<State> states)
        {
            DisplayHeader("States", 15);
            
            int i = 1;
            foreach (var state in states)
            {
                Console.WriteLine($" {i}. {state.StateName}");
                i++;
            }
        }

        public static void DisplayOrderDetails(Order order)
        {
            DisplayHeader("Order Details", 25);

            if (order.OrderID != 0)
                Console.WriteLine($"Order ID: {order.OrderID}");
            
            Console.WriteLine($"Order Status: {order.OrderStatus}");
            Console.WriteLine($"Order Date: {order.OrderDate.ToString("d")}");
            Console.WriteLine($"Customer Name: {order.CustomerName}");
            Console.WriteLine($"Customer State: {order.StateInfo.StateName}");
            Console.WriteLine($"Product: {order.ProductInfo.ProductType}");
            Console.WriteLine($"Area: {order.Area}");
            Console.WriteLine($"Material Cost: {order.TotalMaterialCost:c}");
            Console.WriteLine($"Labor Cost: {order.TotalLaborCost:c}");
            Console.WriteLine($"Tax: {order.Tax:c}");
            Console.WriteLine($"Grand Total: {order.GrandTotal:c}");
        }


        public static void InlineError(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.Gray;
        }
      
    }
}
