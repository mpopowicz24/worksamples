﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.BLL;

namespace FlooringProgram.UI.Utilities
{
    class ReportScreens
    {
        public static void DisplayStatesReport()
        {
            StateManager manager = new StateManager();
            var states = manager.GetAllStates();

            DisplayScreens.DisplayHeader("State Information", 30, ConsoleColor.White);

            Console.WriteLine($"\n{"State",-18}{"Tax Rate",-8}");

            foreach (var state in states.Data)
            {
                Console.WriteLine($"{state.StateName,-18}{state.TaxRate,-8:P}");
            }
            Console.WriteLine();
            UserPrompts.PressKeyForContinue();
        }

        public static void DisplayProductsReport()
        {
            ProductManager manager = new ProductManager();
            var products = manager.GetAllProducts();

            DisplayScreens.DisplayHeader("Product Information", 40, ConsoleColor.White);

            Console.WriteLine($"\n{"",-21}Cost sq/ft");
            Console.WriteLine($"{"Product",-15}{"Material",10}{"Labor",10}");

            foreach (var product in products.Data)
            {
                Console.WriteLine(
                    $"{product.ProductType,-15}{product.CostPerSquareFoot,10:c}{product.LaborCostPerSquareFoot,10:c}");
            }
            Console.WriteLine();
            UserPrompts.PressKeyForContinue();
        }

        public static void DisplayLastThirtyDayOrderReport()
        {
            OrderManager manager = new OrderManager();
            var orderResponse = manager.GetAllActiveOrders(DateTime.Now.Date.AddDays(-30), DateTime.Now.Date);


            if (orderResponse.Success)
            {
                DisplayScreens.DisplayHeader("Last 30 Day Order Report", 85, ConsoleColor.White);
                Console.WriteLine();
                DisplayScreens.DisplayListOfOrdersGroupByDate(orderResponse.Data);
                UserPrompts.PressKeyForContinue();
            }
            else
            {
                DisplayScreens.InlineError(orderResponse.Message);
                UserPrompts.PressKeyForContinue();
            }
        }
    }
}
