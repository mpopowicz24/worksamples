﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Models;

namespace FlooringProgram.UI.Utilities
{
    public class UserPrompts
    {
        public static void PressKeyForContinue()
        {
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }
        
        public static string GetStringFromUser(string message)
        {
            Console.Write(message);
            return Console.ReadLine();
        }

        public static bool ConfirmOrQuit(string message)
        {
            while (true)
            {
                Console.Write(message);
                string input = Console.ReadLine();

                if (input.ToUpper() == "Y")
                    return true;

                if (input.ToUpper() == "N")
                    return false;
            }
        }

        public static char UpdateOrDeletePrompt(string message)
        {
            while (true)
            {
                Console.Write(message);
                string input = Console.ReadLine();

                if (input.ToUpper() == "U")
                    return 'U';
                if (input.ToUpper() == "D")
                    return 'D';
                if (input.ToUpper() == "")
                    return ' ';
            }
        }

        public static DateTime? GetNullableDateFromUser(string message, bool blankIsOK = true)
        {
            do
            {
                Console.Write(message);
                string input = Console.ReadLine();
                DateTime date;

                if (blankIsOK && input == "")
                    return null;

                if (DateTime.TryParse(input, out date))
                    return date;

                DisplayScreens.InlineError("That was not a valid date.");
                PressKeyForContinue();

            } while (true);
        }

        public static DateTime GetDateFromUser(string message, bool blankIsOK = false)
        {
            return (DateTime)GetNullableDateFromUser(message, false);
        }

        public static int GetIntFromUser(string message)
        {
            do
            {
                Console.Write(message);
                string input = Console.ReadLine();
                int value;
                if (int.TryParse(input, out value) && value > 0)
                    return value;
                
                DisplayScreens.InlineError("That was not a valid number.");
                PressKeyForContinue();

            } while (true);
        }

        public static decimal GetDecimalFromUser(string message, bool blankIsOK = false)
        {
            do
            {
                Console.Write(message);
                var input = Console.ReadLine();
                decimal value;

                if (blankIsOK && input == "")
                    return 0;

                if (decimal.TryParse(input, out value) && value > 0)
                    return value;
                
                DisplayScreens.InlineError("That was not a valid value.");
            } while (true);
        }

        public static Product GetProductFromUser(string message, List<Product> products, bool blankIsOK = false )
        {
            DisplayScreens.DisplayListOfProducts(products);
            Console.WriteLine();

            do
            {
                Console.Write(message);
                var input = Console.ReadLine();
                int value;

                if (blankIsOK && input == "")
                    return null;

                if (int.TryParse(input, out value))
                {
                    if (products.ElementAtOrDefault(value-1) != null)
                    {
                        return products[value - 1];
                    }
                }
                DisplayScreens.InlineError("That was not a valid product value.");
            } while (true);
        }

        public static Order GetOrderFromUser(string message, List<Order> orders, bool blankIsOK = false)
        {
            DisplayScreens.DisplayListOfOrders(orders);
            Console.WriteLine();

            do
            {
                Console.Write(message);
                var input = Console.ReadLine();
                int value;

                if (blankIsOK && input == "")
                    return null;

                if (int.TryParse(input, out value))
                {
                    if (orders.FirstOrDefault(o=>o.OrderID == value) != null)
                    {
                        return orders.FirstOrDefault(o => o.OrderID == value);
                    }
                }
                DisplayScreens.InlineError("That was not a valid order number.");
            } while (true);
        }

        public static State GetStateFromUser(string message, List<State> states, bool blankIsOK = false)
        {
            DisplayScreens.DisplayListOfStates(states);
            Console.WriteLine();

            do
            {
                Console.Write(message);
                var input = Console.ReadLine();
                int value;

                if (blankIsOK && input == "")
                    return null;

                if (int.TryParse(input, out value))
                {
                    if (states.ElementAtOrDefault(value - 1) != null)
                    {
                        return states[value - 1];
                    }
                }
                DisplayScreens.InlineError("That was not a valid state.");
            } while (true);
        }

    }
}
