﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlooringProgram.Models
{
    public enum OrderStatus
    {
        Pending,
        Active, 
        Deleted
    }
}
