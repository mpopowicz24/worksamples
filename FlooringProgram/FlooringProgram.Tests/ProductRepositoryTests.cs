﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Data.Repos;
using NUnit.Framework;

namespace FlooringProgram.Tests
{
    [TestFixture]
    class ProductRepositoryTests
    {
        private ProductRepository _repo;

        [SetUp]
        public void BeforeEachTest()
        {
            _repo = new ProductRepository();
        }

        [Test]
        public void CanLoadAllProducts()
        {
            var products = _repo.LoadAllProducts();
            Assert.AreEqual(5, products.Count);
        }

        [Test]
        public void CanLoadProduct()
        {
            var product = _repo.LoadProduct("Wood");
            Assert.AreEqual(5.15m, product.CostPerSquareFoot);
            Assert.AreEqual(4.75m, product.LaborCostPerSquareFoot);
        }

        [Test]
        public void InvalidProductReturnsNull()
        {
            var product = _repo.LoadProduct("Ceramic");
            Assert.IsNull(product);
        }
    }
}
