﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.BLL;
using NUnit.Framework;

namespace FlooringProgram.Tests
{
    [TestFixture]
    class ProductManagerTests
    {
        private ProductManager _manager;

        [SetUp]
        public void BeforeEachTest()
        {
            _manager = new ProductManager();
        }

        [Test]
        public void CanLoadAllProducts()
        {
            var response = _manager.GetAllProducts();
            Assert.AreEqual(5, response.Data.Count);
            Assert.IsTrue(response.Success);
        }
    }
}
