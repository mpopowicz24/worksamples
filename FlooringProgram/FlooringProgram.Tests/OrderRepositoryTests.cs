﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Data.Repos;
using FlooringProgram.Models;
using NUnit.Framework;

namespace FlooringProgram.Tests
{
    [TestFixture]
    class OrderRepositoryTests
    {
        private OrderRepository _repo;
        private Order _testOrder;
        private Order _testOrderWithCommas;


        [SetUp]
        public void BeforeEachTest()
        {
            _repo = new OrderRepository();
            _testOrder = new Order()
            {
                OrderID = 1,
                CustomerName = "Mike's Flooring",
                StateInfo = new State() {StateAbbreviation = "IN", StateName = "Indiana", TaxRate = .05m},
                ProductInfo =
                    new Product() {ProductType = "Cheap Laminate", CostPerSquareFoot = 4m, LaborCostPerSquareFoot = 3m},
                Area = 100,
                TotalMaterialCost = 400m,
                TotalLaborCost = 300m,
                Tax = 35m,
                GrandTotal = 735m,
                OrderDate = DateTime.Parse("2016-5-16"),
                OrderStatus = OrderStatus.Active
            };

            _testOrderWithCommas = new Order()
            {
                OrderID = 1,
                CustomerName = "Mike's Flooring, Inc.",
                StateInfo = new State() { StateAbbreviation = "IN", StateName = "Indiana", TaxRate = .05m },
                ProductInfo =
                    new Product() { ProductType = "Cheap Laminate", CostPerSquareFoot = 4m, LaborCostPerSquareFoot = 3m },
                Area = 100,
                TotalMaterialCost = 400m,
                TotalLaborCost = 300m,
                Tax = 35m,
                GrandTotal = 735m,
                OrderDate = DateTime.Parse("2016-5-16"),
                OrderStatus = OrderStatus.Active
            };
        }
        
        [TestCase("5-16-16", 4)]
        [TestCase("5-12-15", 1)]
        public void CanLoadAllActiveOrdersOnADate(DateTime date, int expected)
        {
            var orders = _repo.LoadAllActiveOrders(date);
            Assert.AreEqual(expected, orders.Count);
        }

        [TestCase("5-15-16", 0)]
        [TestCase("5-15-15", 0)]
        public void LoadAllActiveOrdersReturnsZeroResults(DateTime date, int expected)
        {
            var orders = _repo.LoadAllActiveOrders(date);
            Assert.AreEqual(expected, orders.Count);
        }

        [TestCase("5-16-16", 3, "mike3")]
        [TestCase("5-12-15", 1, "mike2")]
        public void CanLoadOneActiveOrderOnADate(DateTime date, int orderNumber, string expected)
        {
            var order = _repo.LoadActiveOrder(date, orderNumber);
            Assert.AreEqual(expected, order.CustomerName);
        }

        [TestCase("5-16-16", 15)]
        [TestCase("5-12-15", 2)]
        public void LoadActiveOrderReturnsNullIfNotFound(DateTime date, int orderNumber)
        {
            var order = _repo.LoadActiveOrder(date, orderNumber);
            Assert.IsNull(order);
        }

        [Test]
        public void AddNewOrderGetsCorrectID()
        {
            var order = _repo.AddNewOrder(_testOrder);
            Assert.AreEqual(5, order.OrderID);
        }

        [Test]
        public void CommasDontBreakOrder()
        {
            var order = _repo.AddNewOrder(_testOrderWithCommas);

            var newOrder = _repo.LoadActiveOrder(order.OrderDate, order.OrderID);

            Assert.AreEqual("Mike's Flooring, Inc.", newOrder.CustomerName);
        }

        [Test]
        public void AddNewOrderAddsOrderToRepository()
        {
            var order = _repo.AddNewOrder(_testOrder);
            var orders = _repo.LoadAllActiveOrders(_testOrder.OrderDate);

            Assert.AreEqual(5, order.OrderID);
            Assert.AreEqual(5, orders.Count);
        }

        [Test]
        public void UpdateOrderWorks()
        {
            var order = _repo.LoadOrder(new DateTime(2016, 5, 16), 2);
            order.CustomerName = "TestUser";
            order.GrandTotal = 1000m;
            order.StateInfo.StateAbbreviation = "NY";
            order.Tax = 12.50m;
            order.TotalLaborCost = 450m;
            order.TotalMaterialCost = 650m;
            order.Area = 100m;
            order.ProductInfo.ProductType = "Ceramic";

            _repo.UpdateOrder(order);
            var updatedOrder = _repo.LoadOrder(new DateTime(2016, 5, 16), 2);

            Assert.AreEqual("TestUser", updatedOrder.CustomerName);
            Assert.AreEqual(1000m, updatedOrder.GrandTotal);
            Assert.AreEqual("NY", updatedOrder.StateInfo.StateAbbreviation);
            Assert.AreEqual(12.50m, updatedOrder.Tax);
            Assert.AreEqual(450m, updatedOrder.TotalLaborCost);
            Assert.AreEqual(650m, updatedOrder.TotalMaterialCost);
            Assert.AreEqual(100m, updatedOrder.Area);
            Assert.AreEqual("Ceramic", updatedOrder.ProductInfo.ProductType);
        }

        [Test]
        public void DeleteOrderSetsStatusToDeleted()
        {
            var order = _repo.LoadOrder(new DateTime(2016, 5, 16), 2);
            _repo.DeleteOrder(order);
            var deletedOrder = _repo.LoadOrder(new DateTime(2016, 5, 16), 2);

            Assert.AreEqual(OrderStatus.Deleted, deletedOrder.OrderStatus);
        }


    }
}
