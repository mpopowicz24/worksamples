﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.BLL;
using NUnit.Framework;

namespace FlooringProgram.Tests
{
    [TestFixture]
    public class StateManagerTests
    {

        private StateManager _manager;

        [SetUp]
        public void BeforeEachTest()
        {
            _manager = new StateManager();
        }

        [Test]
        public void CanLoadAllStates()
        {
            var response = _manager.GetAllStates();
            Assert.AreEqual(5, response.Data.Count);
            Assert.IsTrue(response.Success);
        }
    }
}
