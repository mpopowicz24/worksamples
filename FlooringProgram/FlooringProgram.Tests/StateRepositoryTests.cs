﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Data.Repos;
using FlooringProgram.Models;
using NUnit.Framework;

namespace FlooringProgram.Tests
{
    [TestFixture]
    class StateRepositoryTests
    {
        private StateRepository _repo;

        [SetUp]
        public void BeforeEachTest()
        {
           _repo = new StateRepository();
        }

        [Test]
        public void CanLoadAllStates()
        {
            var states = _repo.LoadAllStates();
            Assert.AreEqual(5, states.Count);
        }

        [Test]
        public void CanLoadState()
        {
            var state = _repo.LoadState("OH");
            Assert.AreEqual("Ohio", state.StateName);
            Assert.AreEqual(.0625m, state.TaxRate);
        }

        [Test]
        public void InvalidStateReturnsNull()
        {
            var state = _repo.LoadState("WA");
            Assert.IsNull(state);
        }
    }
}
