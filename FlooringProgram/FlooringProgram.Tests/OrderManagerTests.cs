﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.BLL;
using FlooringProgram.Models;
using NUnit.Framework;

namespace FlooringProgram.Tests
{
    [TestFixture]
    class OrderManagerTests
    {
        private OrderManager _manager;
        private Order _testOrder;
        private Order _testPartialOrder;

        [SetUp]
        public void BeforeEachTest()
        {
            _manager = new OrderManager();

            _testOrder = new Order()
            {
                OrderID = 1,
                CustomerName = "Mike's Flooring",
                StateInfo = new State() {StateAbbreviation = "IN", StateName = "Indiana", TaxRate = .05m},
                ProductInfo =
                    new Product() {ProductType = "Cheap Laminate", CostPerSquareFoot = 4m, LaborCostPerSquareFoot = 3m},
                Area = 100,
                TotalMaterialCost = 400m,
                TotalLaborCost = 300m,
                Tax = 35m,
                GrandTotal = 735m,
                OrderDate = DateTime.Parse("2016-5-16"),
                OrderStatus = OrderStatus.Active
            };

            _testPartialOrder = new Order()
            {
                OrderID = 1,
                CustomerName = "Mike's Flooring",
                StateInfo = new State() {StateAbbreviation = "IN", StateName = "Indiana", TaxRate = .05m},
                ProductInfo =
                    new Product() {ProductType = "Cheap Laminate", CostPerSquareFoot = 4m, LaborCostPerSquareFoot = 3m},
                Area = 100,
                OrderDate = DateTime.Parse("2016-5-16"),
                OrderStatus = OrderStatus.Active
            };
        }

        [TestCase("5-16-16", 4)]
        [TestCase("5-12-15", 1)]
        public void CanLoadAllActiveOrdersOnDate(DateTime date, int expected)
        {
            var response = _manager.GetAllActiveOrders(date);
            Assert.AreEqual(expected, response.Data.Count);
            Assert.IsTrue(response.Success);
        }

        [TestCase("5-15-16")]
        [TestCase("5-15-15")]
        public void NoOrdersOnDateReturnsMessage(DateTime date)
        {
            var response = _manager.GetAllActiveOrders(date);
            Assert.AreEqual("No orders found on that date.", response.Message);
            Assert.IsFalse(response.Success);
        }

        [TestCase("5-16-16", 1, "mike")]
        [TestCase("5-12-15", 1, "mike2")]
        public void CanLoadActiveOrder(DateTime date, int orderNumber, string expectedName)
        {
            var response = _manager.GetActiveOrder(date, orderNumber);
            Assert.AreEqual(expectedName, response.Data.CustomerName);
            Assert.IsTrue(response.Success);
        }

        [TestCase("5-16-16", 5)]
        [TestCase("5-12-15", 6)]
        public void NoActiveOrderReturnsMessage(DateTime date, int orderNumber)
        {
            var response = _manager.GetActiveOrder(date, orderNumber);
            Assert.IsFalse(response.Success);
            Assert.AreEqual("Order couldn't be found.", response.Message);
        }

        [Test]
        public void CanAddOrder()
        {
            var addResponse = _manager.AddNewOrder(_testOrder);
            var orderResponse = _manager.GetActiveOrder(addResponse.Data.OrderDate, addResponse.Data.OrderID);

            Assert.IsTrue(addResponse.Success);
            Assert.IsTrue(orderResponse.Success);
        }

        [Test]
        public void CanDeleteOrder()
        {
            var orderResponse = _manager.GetActiveOrder(new DateTime(2016, 5, 16), 2);

            var deleteResponse = _manager.DeleteOrder(orderResponse.Data);

            var retryOrderResponse = _manager.GetActiveOrder(new DateTime(2016, 5, 16), 2);

            Assert.IsTrue(orderResponse.Success);
            Assert.IsTrue(deleteResponse.Success);
            Assert.IsFalse(retryOrderResponse.Success);
        }

        [Test]
        public void CanUpdateOrderWithSameDate()
        {
            var orderResponse = _manager.GetActiveOrder(new DateTime(2016, 5, 16), 2);
            var order = orderResponse.Data;

            order.CustomerName = "TestUser";
            order.GrandTotal = 1000m;
            order.StateInfo.StateAbbreviation = "NY";
            order.Tax = 12.50m;
            order.TotalLaborCost = 450m;
            order.TotalMaterialCost = 650m;
            order.Area = 100m;
            order.ProductInfo.ProductType = "Ceramic";

            var updateResponse = _manager.UpdateOrder(order);

            var retryOrderResponse = _manager.GetActiveOrder(new DateTime(2016, 5, 16), 2);
            var updatedOrder = retryOrderResponse.Data;

            Assert.IsTrue(orderResponse.Success);
            Assert.IsTrue(updateResponse.Success);
            Assert.IsTrue(retryOrderResponse.Success);
            Assert.AreEqual("TestUser", updatedOrder.CustomerName);
            Assert.AreEqual(1000m, updatedOrder.GrandTotal);
            Assert.AreEqual("NY", updatedOrder.StateInfo.StateAbbreviation);
            Assert.AreEqual(12.50m, updatedOrder.Tax);
            Assert.AreEqual(450m, updatedOrder.TotalLaborCost);
            Assert.AreEqual(650m, updatedOrder.TotalMaterialCost);
            Assert.AreEqual(100m, updatedOrder.Area);
            Assert.AreEqual("Ceramic", updatedOrder.ProductInfo.ProductType);
        }

        [Test]
        public void CanUpdateOrderWithDifferentDate()
        {
            var orderResponse = _manager.GetActiveOrder(new DateTime(2016, 5, 16), 2);
            var order = orderResponse.Data;

            order.CustomerName = "TestUser";
            order.GrandTotal = 1000m;
            order.StateInfo.StateAbbreviation = "NY";
            order.Tax = 12.50m;
            order.TotalLaborCost = 450m;
            order.TotalMaterialCost = 650m;
            order.Area = 100m;
            order.ProductInfo.ProductType = "Ceramic";

            var updateResponse = _manager.UpdateOrder(order, new DateTime(2015, 5, 12));

            var oldOrderResponse = _manager.GetOrder(new DateTime(2016, 5, 16), 2);

            var retryOrderResponse = _manager.GetActiveOrder(new DateTime(2015, 5, 12), 2);
            var updatedOrder = retryOrderResponse.Data;

            Assert.IsTrue(orderResponse.Success);
            Assert.IsTrue(updateResponse.Success); //order is in new date
            Assert.IsTrue(retryOrderResponse.Success);
            Assert.IsTrue(oldOrderResponse.Success);

            //delete order from old date
            Assert.AreEqual(OrderStatus.Deleted, oldOrderResponse.Data.OrderStatus);

            //updated order details are correct
            Assert.AreEqual("TestUser", updatedOrder.CustomerName);
            Assert.AreEqual(1000m, updatedOrder.GrandTotal);
            Assert.AreEqual("NY", updatedOrder.StateInfo.StateAbbreviation);
            Assert.AreEqual(12.50m, updatedOrder.Tax);
            Assert.AreEqual(450m, updatedOrder.TotalLaborCost);
            Assert.AreEqual(650m, updatedOrder.TotalMaterialCost);
            Assert.AreEqual(100m, updatedOrder.Area);
            Assert.AreEqual("Ceramic", updatedOrder.ProductInfo.ProductType);
        }

        [Test]
        public void CalculateOrderDetailsWorks()
        {
            var updatedOrderTotals = _manager.CalculateOrderDetails(_testPartialOrder);

            Assert.AreEqual(35m, updatedOrderTotals.Tax);
            Assert.AreEqual(300m, updatedOrderTotals.TotalLaborCost);
            Assert.AreEqual(400m, updatedOrderTotals.TotalMaterialCost);
            Assert.AreEqual(735m, updatedOrderTotals.GrandTotal);
        }
    }
}
