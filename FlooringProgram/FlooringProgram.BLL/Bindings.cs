﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Data.Interfaces;
using FlooringProgram.Data.Repos;
using FlooringProgram.Models;
using Ninject.Modules;

namespace FlooringProgram.BLL
{
    class Bindings : NinjectModule
    {
        public override void Load()
        {
            //add logic to read app.config file

            Bind<IOrderRepository>().To<OrderRepository>();
        }
    }
}
