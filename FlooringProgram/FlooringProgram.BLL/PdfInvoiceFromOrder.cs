﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Models;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;

namespace FlooringProgram.BLL
{
    public class PdfInvoiceFromOrder
    {
        private const string _logoPath = "DataFiles/logo.png";

        public static Document CreateDocument(Order order)
        {
            Document document = new Document();

            // Get the page size
            Unit width, height;
            PageSetup.GetPageSize(PageFormat.Letter, out width, out height);

            // Add a section to the document and configure it such that it will be in the centre
            // of the page
            Section section = document.AddSection();
            section.PageSetup.PageHeight = height - 10;
            section.PageSetup.PageWidth = width - 10;
            section.PageSetup.LeftMargin = 5;
            section.PageSetup.RightMargin = 5;
            section.PageSetup.TopMargin = 10;

            // Header Table
            Table headerTable = new Table();
            headerTable.Borders.Width = 1;
            var column = headerTable.AddColumn(width);
            column.Format.Alignment = ParagraphAlignment.Center;

            Font font = new Font("Times New Roman", 36);

            // Add a row
            Row row = headerTable.AddRow();
            row.BottomPadding = 10;
            Cell cell = row.Cells[0];

            cell.Format.Font.Color = Colors.Black;
            cell.Format.Font.ApplyFont(font);
            cell.Borders.Top.Visible = false;
            cell.Borders.Left.Visible = false;
            cell.Borders.Right.Visible = false;
            cell.Borders.Bottom.Visible = true;

            cell.AddParagraph("SG Flooring Corporation");
            cell.AddParagraph("Invoice");

            //order details table
            Table orderInfoTable = new Table();
            orderInfoTable.Rows.LeftIndent = 20;
            var orderInfocolumn = orderInfoTable.AddColumn(width);
            orderInfocolumn.Format.Alignment = ParagraphAlignment.Left;

            Font orderInfofont = new Font("Times New Roman", 22);

            // Add a row 
            Row orderInforow = orderInfoTable.AddRow();
            orderInforow.TopPadding = 10;

            Cell orderInfocell = orderInforow.Cells[0];

            orderInfocell.Format.Font.Color = Colors.Black;
            orderInfocell.Format.Font.ApplyFont(orderInfofont);

            orderInfocell.AddParagraph($"Date: {order.OrderDate.ToString("d")}");
            orderInfocell.AddParagraph($"Order Number: {order.OrderID}");
            orderInfocell.AddParagraph($"Customer: {order.CustomerName}");
            orderInfocell.AddParagraph($"Customer State: {order.StateInfo.StateName}");
            orderInfocell.AddParagraph($"Product: {order.ProductInfo.ProductType}");
            orderInfocell.AddParagraph($"Area: {order.Area}");
            orderInfocell.AddParagraph($"Material Cost: {order.TotalMaterialCost:c}");
            orderInfocell.AddParagraph($"Labor Cost: {order.TotalLaborCost:c}");
            orderInfocell.AddParagraph($"Tax: {order.Tax:c}");
            orderInfocell.AddParagraph($"Grand Total: {order.GrandTotal:c}");
            

            //// Create the order details table

            //Font orderDetailsFont = new Font("Times New Roman", 14);

            //Table orderDetailsTable = new Table();
            //orderDetailsTable.Style = "Table";
            //orderDetailsTable.Rows.LeftIndent = 0;
            //orderDetailsTable.Format.Font.ApplyFont(orderDetailsFont);


            //Column orderDetailsColumn = orderDetailsTable.AddColumn(Unit.FromInch(1.4));
            //orderDetailsColumn.Format.Alignment = ParagraphAlignment.Center;

            //orderDetailsColumn = orderDetailsTable.AddColumn(Unit.FromInch(1.25));
            //orderDetailsColumn.Format.Alignment = ParagraphAlignment.Right;

            //orderDetailsColumn = orderDetailsTable.AddColumn(Unit.FromInch(1.35));
            //orderDetailsColumn.Format.Alignment = ParagraphAlignment.Right;

            //orderDetailsColumn = orderDetailsTable.AddColumn(Unit.FromInch(1.35));
            //orderDetailsColumn.Format.Alignment = ParagraphAlignment.Right;

            //orderDetailsColumn = orderDetailsTable.AddColumn(Unit.FromInch(1.05));
            //orderDetailsColumn.Format.Alignment = ParagraphAlignment.Center;

            //orderDetailsColumn = orderDetailsTable.AddColumn(Unit.FromInch(1.25));
            //orderDetailsColumn.Format.Alignment = ParagraphAlignment.Center;

            //orderDetailsTable.Rows.LeftIndent = 10;

            //// Create the header of the table
            //Row orderDetailsrow = orderDetailsTable.AddRow();
            //orderDetailsrow.HeadingFormat = true;
            //orderDetailsrow.Format.Alignment = ParagraphAlignment.Center;
            //orderDetailsrow.Format.Font.Bold = true;
            //orderDetailsrow.Borders.Bottom.Width = 1;
            //orderDetailsrow.Borders.Bottom.Color = Colors.Black;
            //orderDetailsrow.BottomPadding = 5;


            //orderDetailsrow.Cells[0].AddParagraph("Product");
            //orderDetailsrow.Cells[0].Format.Alignment = ParagraphAlignment.Left;
            //orderDetailsrow.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            //orderDetailsrow.Cells[1].AddParagraph("Area (sq ft)");
            //orderDetailsrow.Cells[1].Format.Alignment = ParagraphAlignment.Left;
            //orderDetailsrow.Cells[2].AddParagraph("Material Cost");
            //orderDetailsrow.Cells[2].Format.Alignment = ParagraphAlignment.Left;
            //orderDetailsrow.Cells[3].AddParagraph("Labor Cost");
            //orderDetailsrow.Cells[3].Format.Alignment = ParagraphAlignment.Left;
            //orderDetailsrow.Cells[4].AddParagraph("Tax");
            //orderDetailsrow.Cells[4].Format.Alignment = ParagraphAlignment.Left;
            //orderDetailsrow.Cells[5].AddParagraph("Total");
            //orderDetailsrow.Cells[5].Format.Alignment = ParagraphAlignment.Left;

            //// Create the details of the table
            //orderDetailsrow = orderDetailsTable.AddRow();
            //orderDetailsrow.HeadingFormat = true;
            //orderDetailsrow.Format.Alignment = ParagraphAlignment.Center;
            //orderDetailsrow.Format.Font.Bold = false;
            //orderDetailsrow.TopPadding = 5;


            //orderDetailsrow.Cells[0].AddParagraph($"{order.ProductInfo.ProductType}");
            //orderDetailsrow.Cells[0].Format.Alignment = ParagraphAlignment.Left;
            //orderDetailsrow.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            //orderDetailsrow.Cells[1].AddParagraph($"{order.Area}");
            //orderDetailsrow.Cells[1].Format.Alignment = ParagraphAlignment.Left;
            //orderDetailsrow.Cells[2].AddParagraph($"{order.TotalMaterialCost:c}");
            //orderDetailsrow.Cells[2].Format.Alignment = ParagraphAlignment.Left;
            //orderDetailsrow.Cells[3].AddParagraph($"{order.TotalLaborCost:c}");
            //orderDetailsrow.Cells[3].Format.Alignment = ParagraphAlignment.Left;
            //orderDetailsrow.Cells[4].AddParagraph($"{order.Tax:c}");
            //orderDetailsrow.Cells[4].Format.Alignment = ParagraphAlignment.Left;
            //orderDetailsrow.Cells[5].AddParagraph($"{order.GrandTotal:c}");
            //orderDetailsrow.Cells[5].Format.Alignment = ParagraphAlignment.Left;


            Image img = section.AddImage(_logoPath);
            img.Left = ShapePosition.Center;
            section.Add(headerTable);
            section.Add(orderInfoTable);
            //section.Add(orderDetailsTable);

            return document;
        }

    }
}
