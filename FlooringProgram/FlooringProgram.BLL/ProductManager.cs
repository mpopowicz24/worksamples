﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Data.Factories;
using FlooringProgram.Data.Interfaces;
using FlooringProgram.Models;

namespace FlooringProgram.BLL
{
    public class ProductManager
    {
        private IProductRepository _repo;
        private ILogRepository _logRepo;

        public ProductManager()
        {
            _repo = ProductRepositoryFactory.GetProductRepository();
            _logRepo = LogRepositoryFactory.GetLogRepository();
        }

        public Response<List<Product>> GetAllProducts()
        {
         
            var result = new Response<List<Product>>();

            try
            {
                var products = _repo.LoadAllProducts();

                if (products == null)
                {
                    result.Success = false;
                    result.Message = "No products found.";
                }
                else
                {
                    result.Success = true;
                    result.Data = products;
                }

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "There was an error accessing the products. Please try again later.";
                _logRepo.WriteToExceptionLog(ex);
            }

            return result;
        }
    }
    
}
