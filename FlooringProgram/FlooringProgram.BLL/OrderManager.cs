﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Data;
using FlooringProgram.Data.Factories;
using FlooringProgram.Data.Interfaces;
using FlooringProgram.Data.Repos;
using FlooringProgram.Models;
using MigraDoc.DocumentObjectModel;
using Ninject;

namespace FlooringProgram.BLL
{
    public class OrderManager
    {
        private IOrderRepository _repo;
        private ILogRepository _logRepo;
        private IInvoiceWriter _invoiceWriter;

        public OrderManager()
        {
            _repo = OrderRepositoryFactory.GetOrderRepository();
            _logRepo = LogRepositoryFactory.GetLogRepository();
            _invoiceWriter = InvoiceWriterFactory.GetInvoiceWriter();
        }
        
        public Response<List<Order>> GetAllActiveOrders(DateTime date)
        {
            var result = new Response<List<Order>>();

            try
            {
                var orders = _repo.LoadAllActiveOrders(date);

                if (orders.Count == 0)
                {
                    result.Success = false;
                    result.Message = "No orders found on that date.";
                }
                else
                {
                    result.Success = true;
                    result.Data = orders;
                }

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "There was an error accessing the orders. Please try again later.";
                _logRepo.WriteToExceptionLog(ex);
            }

            return result;
        }

        public Response<List<Order>> GetAllActiveOrders(DateTime startDate, DateTime endDate)
        {
            var result = new Response<List<Order>>();

            try
            {
                var orders = _repo.LoadAllActiveOrdersInDateRange(startDate, endDate);

                if (orders.Count == 0)
                {
                    result.Success = false;
                    result.Message = "No orders found in date range.";
                }
                else
                {
                    result.Success = true;
                    result.Data = orders;
                }

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "There was an error accessing the orders. Please try again later.";
                _logRepo.WriteToExceptionLog(ex);
            }

            return result;
        }

        public Response<Order> AddNewOrder(Order order)
        {
            var result = new Response<Order>();

            try
            {
                var addedOrder = _repo.AddNewOrder(order);

                if (addedOrder == null)
                {
                    result.Success = false;
                    result.Message = "Order couldn't be added";
                }
                else
                {
                    result.Success = true;
                    result.Data = addedOrder;
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "There was an error accessing the orders. Please try again later.";
                _logRepo.WriteToExceptionLog(ex);
            }

            return result;
        }

        public Response<Order> GetOrder(DateTime date, int orderNumber)
        {
            var result = new Response<Order>();

            try
            {
                var order = _repo.LoadOrder(date, orderNumber);

                if (order == null)
                {
                    result.Success = false;
                    result.Message = "Order couldn't be found";
                }
                else
                {
                    result.Success = true;
                    result.Data = order;
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "There was an error accessing the orders. Please try again later.";
                _logRepo.WriteToExceptionLog(ex);
            }

            return result;
        }

        public Response<Order> GetActiveOrder(DateTime date, int orderNumber)
        {
            var result = new Response<Order>();

            try
            {
                var order = _repo.LoadActiveOrder(date, orderNumber);

                if (order == null)
                {
                    result.Success = false;
                    result.Message = "Order couldn't be found.";
                }
                else
                {
                    result.Success = true;
                    result.Data = order;
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "There was an error accessing the orders. Please try again later.";
                _logRepo.WriteToExceptionLog(ex);
            }

            return result;
        }

        public Response<Order> UpdateOrder(Order order, DateTime? newDate = null)
        {
            var result = new Response<Order>();

            if (newDate.HasValue)
            {
                DateTime oldDate = order.OrderDate;
                order.OrderDate = (DateTime)newDate;

                Response<Order> addOrderResponse = AddNewOrder(order);

                if (addOrderResponse.Success)
                {
                    order.OrderDate = oldDate;
                    Response<Order> deleteOrderResponse = DeleteOrder(order);

                    if (deleteOrderResponse.Success)
                    {
                        result.Success = true;
                        order.OrderDate = (DateTime)newDate;
                        result.Data = addOrderResponse.Data;
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Update failed";

                        Response<Order> deleteNewOrderResponse = DeleteOrder(order);

                        if (!deleteNewOrderResponse.Success)
                            result.Message = "Update error - could not remove order from old date file. Order exists in two dates";
                    }
                }
                else
                {
                    result.Success = false;
                    result.Message = "Update failed";
                }

            }
            else
            {
                try
                {
                    var updatedOrder = _repo.UpdateOrder(order);
                    result.Success = true;
                    result.Data = updatedOrder;
                    
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = "There was an error updating the order. Please try again later.";
                    _logRepo.WriteToExceptionLog(ex);
                }
            }

            return result;
        }

        public Response<Order> DeleteOrder(Order order)
        {
            var result = new Response<Order>();

            try
            {
                _repo.DeleteOrder(order);
                result.Success = true;
                result.Data = order;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "There was an error deleting the order. Please try again later.";
                _logRepo.WriteToExceptionLog(ex);
            }

            return result;
        }

        public void CreatePDF(Order order)
        {
            Document document = PdfInvoiceFromOrder.CreateDocument(order);

            _invoiceWriter.CreatePdfInvoice(document, order);
        }


        public Order CalculateOrderDetails(Order order)
        {
            order.TotalMaterialCost = order.Area*order.ProductInfo.CostPerSquareFoot;
            order.TotalLaborCost = order.Area * order.ProductInfo.LaborCostPerSquareFoot;
            order.Tax = (order.TotalMaterialCost + order.TotalLaborCost) * order.StateInfo.TaxRate;
            order.GrandTotal = (order.TotalMaterialCost + order.TotalLaborCost + order.Tax);

            return order;
        }
    }
}
