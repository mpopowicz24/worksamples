﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Data.Factories;
using FlooringProgram.Data.Interfaces;
using FlooringProgram.Models;

namespace FlooringProgram.BLL
{
    public class StateManager
    {
        private IStateRepository _repo;
        private ILogRepository _logRepo;

        public StateManager()
        {
            _repo = StateRepositoryFactory.GetStateRepository();
            _logRepo = LogRepositoryFactory.GetLogRepository();
        }
        
        public Response<List<State>> GetAllStates()
        {
            Response<List<State>> result = new Response<List<State>>();

            try
            {
                var states = _repo.LoadAllStates();

                if (states.Count == 0)
                {
                    result.Success = false;
                    result.Message = "State information is missing";                    
                }
                else
                {
                    result.Success = true;
                    result.Data = states;
                }

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "There was an error accessing the state data. Please try again later.";
                _logRepo.WriteToExceptionLog(ex);
            }

            return result;
        }
    }
}
