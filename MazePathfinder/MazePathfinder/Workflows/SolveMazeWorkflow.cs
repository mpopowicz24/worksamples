﻿using MazePathfinder.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MazePathfinder.Utilities;

namespace MazePathfinder.Workflows
{
    public class SolveMazeWorkflow
    {
        public void Execute(List<MazeRequest> mazes)
        {
            do
            {
                Console.Clear();

                OutputScreens.DrawBoard(mazes[0].Maze, mazes[0].Width, mazes[0].Height);

                Console.WriteLine("\n1. Show mazes being solved");
                Console.WriteLine("2. Show solutions only");

                Console.WriteLine("\n(Q) Back to Maze Selection");

                Console.Write("\nEnter Choice: ");
                string input = Console.ReadLine();

                if (input.ToUpper() == "Q")
                    break;

                bool resultsShown = ProcessChoice(input, mazes);

                if (resultsShown)
                    break;

            } while (true);
        }

        static private bool ProcessChoice(string choice, List<MazeRequest> mazes)
        {
            switch (choice)
            {
                case "1":
                    for (int i=0; i < mazes.Count; i++)                    
                        mazes[i].Solution = MazeSolver.FindPath(mazes[i], true);

                    OutputScreens.DisplayResults(mazes);
                    return true;                    
                case "2":
                    for (int i = 0; i < mazes.Count; i++)
                        mazes[i].Solution = MazeSolver.FindPath(mazes[i]);

                    OutputScreens.DisplayResults(mazes);
                    return true;                    
            }

            return false;
        }
    }
}
