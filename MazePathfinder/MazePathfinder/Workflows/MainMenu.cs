﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazePathfinder.Workflows
{
    public class MainMenu
    {
        static public void Execute()
        {
            do
            {
                Console.Clear();
                Console.WriteLine("Welcome Maze Pathfinder");
                Console.WriteLine("====================================");
                Console.WriteLine("\n1. Use Existing Maze");
                Console.WriteLine("2. Create a New Maze");                
                Console.WriteLine("\n(Q) to Quit");

                Console.Write("\nEnter Choice: ");
                string input = Console.ReadLine();                 

                if (input.ToUpper() == "Q")
                    break;

                ProcessChoice(input);

            } while (true);
        }

        static private void ProcessChoice(string choice)
        {
            switch (choice)
            {
                case "1":
                    FileLookupWorkflow fileLookupWF = new FileLookupWorkflow();
                    fileLookupWF.Execute();
                    break;
                case "2":
                    ConsoleInputWorkflow consoleInputWF = new ConsoleInputWorkflow();
                    consoleInputWF.Execute();
                    break;
            }
        }
    }
}
