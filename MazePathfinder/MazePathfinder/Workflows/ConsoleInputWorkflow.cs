﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MazePathfinder.Utilities;
using MazePathfinder.Models;

namespace MazePathfinder.Workflows
{
    public class ConsoleInputWorkflow
    {
        public void Execute()
        {
            Console.Clear();
            Console.WriteLine("Create a Maze");
            Console.WriteLine("====================================\n");

            int width = UserPrompts.GetIntFromUser("Enter Maze Width: ");
            int height = UserPrompts.GetIntFromUser("Enter Maze Height: ");

            string[] mazeRows = new string[height];

            for (int i=0; i < height; i++)
            {
                while (true)
                {
                    Console.Write($"Line {i + 1}: ");
                    string row = Console.ReadLine();

                    if (row.Length == width && row.All(c => "01".Contains(c)))
                    {
                        mazeRows[i] = row;
                        break;
                    }                        
                }                
            }

            List<MazeRequest> mazes = MazeCreator.CreateMazeList(mazeRows, width, height);

            SolveMazeWorkflow solveMazeWF = new SolveMazeWorkflow();
            solveMazeWF.Execute(mazes);

        }
    }
}
