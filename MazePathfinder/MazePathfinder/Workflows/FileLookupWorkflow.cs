﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MazePathfinder.Utilities;
using MazePathfinder.Models;

namespace MazePathfinder.Workflows
{
    public class FileLookupWorkflow
    {             
        public void Execute()
        {                     
            Dictionary<int, string> fileNames = FileOperations.GetFileNames();
            DisplayFilesScreen(fileNames);            
        }

        private static void DisplayFilesScreen(Dictionary<int, string> fileNames)
        {
            do
            {
                Console.Clear();
                Console.WriteLine("Select a Maze to Solve");
                Console.WriteLine("====================================");
                foreach (var item in fileNames)
                {
                    Console.WriteLine($"{item.Key}. {item.Value}");
                }
                
                Console.WriteLine("\n(Q) Back to Main Menu");

                Console.Write("\nEnter Choice: ");
                string input = Console.ReadLine();             
               
                if (input.ToUpper() == "Q")
                    break;

                ProcessChoice(input, fileNames);

            } while (true);

        }

        static private void ProcessChoice(string choice, Dictionary<int, string> fileNames)
        {
            int selection;

            if (int.TryParse(choice, out selection) && fileNames.ContainsKey(selection))
            {
                List<MazeRequest> mazes = FileOperations.GenerateMazesFromFile(fileNames[selection]);
                                
                SolveMazeWorkflow solveMazeWF = new SolveMazeWorkflow();
                solveMazeWF.Execute(mazes);
            }
        }


    }
}
