﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MazePathfinder.Workflows;

namespace MazePathfinder
{
    class Program
    {               
        static void Main(string[] args)
        {
            MainMenu.Execute();              
        }
    }
}
