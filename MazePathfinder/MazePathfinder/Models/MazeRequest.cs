﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazePathfinder.Models
{
    public class MazeRequest
    {
        public int[,] Maze { get; set; }
        public bool[,] VisitedMaze { get; set; }
        public int StartX { get; set; }
        public int StartY { get; set; }
        public int EndX { get; set; }
        public int EndY { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public List<Tuple<int, int>> StartingPoints { get; set; }
        public string Solution { get; set; }
    }
}
