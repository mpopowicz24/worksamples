﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MazePathfinder.Utilities;
using MazePathfinder.Models;

namespace MazePathfinder.Utilities
{
    public class MazeSolver
    {
        static Stack<Coordinate> coordinates = new Stack<Coordinate>();

        public static string FindPath(MazeRequest maze, bool showMoves = false)
        {
            coordinates.Clear();  

            bool endIsReached = false;
            coordinates.Push(new Coordinate(maze.StartX, maze.StartY));
            maze.VisitedMaze[maze.StartX, maze.StartY] = true;
            maze.Maze[maze.StartX, maze.StartY] = 3;

            while (!endIsReached)
            {
                if (coordinates.Count == 0)                
                    return "No Solution";                
                                
                //search for surrounding coordinates that haven't been visited
                Coordinate coord = GetNextCoordinate(maze.Maze, maze.VisitedMaze, coordinates.Peek(), maze.Width, maze.Height);

                //add to stack
                if (coord != null)
                {
                    maze.Maze[coord.CoordX, coord.CoordY] = 3; //used to display final path

                    if (showMoves)
                        ShowEachMove(maze);

                    if (coord.CoordX == maze.EndX && coord.CoordY == maze.EndY)
                    {
                        coordinates.Push(coord);
                        endIsReached = true;
                        break;
                    }

                    coordinates.Push(coord);
                    maze.VisitedMaze[coord.CoordX, coord.CoordY] = true;
                }
                else
                {
                    //if no coordinates found then go backwards

                    maze.Maze[coordinates.Peek().CoordX, coordinates.Peek().CoordY] = 1;

                    if (showMoves)
                        ShowEachMove(maze);

                    coordinates.Pop();
                }
            }

            //reverse the stack into another stack
            Stack<Coordinate> correctOrderCoordinates = new Stack<Coordinate>();

            foreach (var coordinate in coordinates)
            {
                correctOrderCoordinates.Push(coordinate);
            }

            return FormatAnswer(correctOrderCoordinates);
        }

        private static void ShowEachMove(MazeRequest maze)
        {
            Thread.Sleep(150);
            Console.Clear();
            Console.WriteLine("Small maze start at {0},{1}\n", maze.StartX, maze.StartY);
            OutputScreens.DrawBoard(maze.Maze, maze.Width, maze.Height);
        }
        
        private static string FormatAnswer(Stack<Coordinate> correctOrderCoordinates)
        {
            //pop off first spot of maze
            correctOrderCoordinates.Pop();

            char currentDirection = correctOrderCoordinates.Peek().Direction;
            int iterator = 0;
            string result = "";

            //create string answer
            foreach (var coordinate in correctOrderCoordinates)
            {
                if (coordinate.Direction == currentDirection)
                {
                    iterator++;
                }
                else
                {
                    result += string.Format($"{iterator}{currentDirection}");
                    currentDirection = coordinate.Direction;
                    iterator = 1;
                }
            }
            //add last leg to answer string
            result += string.Format($"{iterator}{currentDirection}");
            return result;
        }

        private static Coordinate GetNextCoordinate(int[,] maze, bool[,] visitedMaze, Coordinate coord, int width, int height)
        {
            int coordX = coord.CoordX;
            int coordY = coord.CoordY;

            if (coordY - 1 >= 0 && maze[coordX, coordY - 1] == 1 && visitedMaze[coordX, coordY - 1] == false)
                return new Coordinate(coordX, coordY - 1) { Direction = 'U' };

            if (coordY + 1 < height && maze[coordX, coordY + 1] == 1 && visitedMaze[coordX, coordY + 1] == false)
                return new Coordinate(coordX, coordY + 1) { Direction = 'D' };

            if (coordX - 1 >= 0 && maze[coordX - 1, coordY] == 1 && visitedMaze[coordX - 1, coordY] == false)
                return new Coordinate(coordX - 1, coordY) { Direction = 'L' };

            if (coordX + 1 < width && maze[coordX + 1, coordY] == 1 && visitedMaze[coordX + 1, coordY] == false)
                return new Coordinate(coordX + 1, coordY) { Direction = 'R' };

            return null;
        }
    }
}
