﻿using MazePathfinder.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazePathfinder.Utilities
{
    public class MazeCreator
    {

        public static List<MazeRequest> CreateMazeList(string[] mazeRows, int width, int height)
        {
            List<MazeRequest> mazes = new List<MazeRequest>();

            MazeRequest maze1 = new MazeRequest();
            MazeRequest maze2 = new MazeRequest();
            MazeRequest maze3 = new MazeRequest();


            maze1 = CreateMazeObject(maze1, mazeRows, width, height);
            maze2 = CreateMazeObject(maze2, mazeRows, width, height);
            maze3 = CreateMazeObject(maze3, mazeRows, width, height);

            maze1.StartX = maze1.Width - 1;
            maze1.StartY = 0;

            maze2.StartX = 0;
            maze2.StartY = maze2.Height - 1;

            maze3.StartX = maze3.Width - 1;
            maze3.StartY = maze3.Height - 1;

            mazes.AddRange(new List<MazeRequest> { maze1, maze2, maze3 });

            return mazes;
        }


        private static MazeRequest CreateMazeObject(MazeRequest maze, string[] mazeRows, int width, int height)
        {
            maze.Width = width;
            maze.Height = height;
            maze.EndX = 0;
            maze.EndY = 0;
            maze.StartX = maze.Width - 1;
            maze.StartY = 0;

            maze.Maze = new int[maze.Width, maze.Height];
            maze.VisitedMaze = new bool[maze.Width, maze.Height];

            for (int row = 0; row < mazeRows.Length; row++)
            {
                for (int col = 0; col < mazeRows[row].Length; col++)
                {
                    maze.Maze[col, row] = int.Parse(mazeRows[row][col].ToString());
                    maze.VisitedMaze[col, row] = false;
                }
            }

            return maze;
        }
    }
}
