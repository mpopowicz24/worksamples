﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MazePathfinder.Models;

namespace MazePathfinder.Utilities
{
    public class FileOperations
    {
        static private string _mazeDirectory = @"MazeFiles\";

        static public Dictionary<int, string> GetFileNames()
        {
            Dictionary<int, string> fileNames = new Dictionary<int, string>();

            DirectoryInfo dirInfo = new DirectoryInfo(_mazeDirectory);

            int key = 1;

            foreach (FileInfo file in dirInfo.GetFiles("*.txt"))
            {
                fileNames.Add(key, file.Name);
                key++;
            }

            return fileNames;
        }

        static public List<MazeRequest> GenerateMazesFromFile(string filename)
        {

            var rows = File.ReadAllLines(_mazeDirectory + "\\" + filename);

            int width = int.Parse(rows[0].Substring(0, rows[0].IndexOf(',')));
            int height = int.Parse(rows[0].Substring(rows[0].IndexOf(',') + 1));
            
            string[] mazeRows = rows.Skip(1).ToArray();

            return MazeCreator.CreateMazeList(mazeRows, width, height);
            
        }

        

    }
}
