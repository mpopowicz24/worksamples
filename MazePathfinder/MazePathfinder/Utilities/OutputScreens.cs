﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MazePathfinder.Models;

namespace MazePathfinder.Utilities
{
    public class OutputScreens
    {
        public static void DrawBoard(int[,] maze, int width, int height)
        {
            for (int row = 0; row < height; row++)
            {
                for (int col = 0; col < width; col++)
                {
                    if (maze[col, row] == 3)
                        Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write($"{maze[col, row]} ");

                    Console.ForegroundColor = ConsoleColor.Gray;
                }
                Console.WriteLine();
            }
        }

        public static void DisplayResults(List<MazeRequest> mazes)
        {
            Console.Clear();
            Console.WriteLine("Maze Results");
            Console.WriteLine("====================================\n");
           
            foreach(MazeRequest maze in mazes)
            {
                Console.WriteLine($"Maze starting at: {maze.StartX},{maze.StartY}");
                Console.WriteLine($"Solution: {maze.Solution}\n");
                DrawBoard(maze.Maze, maze.Width, maze.Height);                
                Console.WriteLine("\n\n");
            }

            Console.WriteLine("Press any key to continue");
            Console.ReadKey();
        }
    }
}
