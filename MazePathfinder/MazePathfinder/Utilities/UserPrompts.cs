﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazePathfinder.Utilities
{
    public class UserPrompts
    {
        public static int GetIntFromUser(string message)
        {

            do
            {
                Console.Write(message);
                string input = Console.ReadLine();
                int value;
                if (int.TryParse(input, out value))
                    return value;

                Console.WriteLine("That was not a valid number.");

                Console.WriteLine("Press any key to continue...");
                Console.ReadKey();

            } while (true);
        }
    }
}
